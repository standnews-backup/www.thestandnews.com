<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>https://www.thestandnews.com/author/sunnyhengchun/rss</id>
    <title>立場新聞 Stand News - 林艾德</title>
    <updated>2021-12-17T21:34:54.751Z</updated>
    <generator>https://github.com/jpmonette/feed</generator>
    <link rel="alternate" href="https://www.thestandnews.com/author/sunnyhengchun"/>
    <subtitle>《立場新聞》編採獨立自主，不受任何贊助人、財團、權力機構及黨派左右。我們以非牟利原則營運，所有經營盈餘和贊助，只會用於傳媒事業。</subtitle>
    <rights>© 2021 立場新聞. All rights reserved.</rights>
    <entry>
        <title type="html"><![CDATA[「改變現況」的公投，現況真的令人無法接受嗎？]]></title>
        <id>https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E6%94%B9%E8%AE%8A%E7%8F%BE%E6%B3%81%E7%9A%84%E5%85%AC%E6%8A%95%E7%8F%BE%E6%B3%81%E7%9C%9F%E7%9A%84%E4%BB%A4%E4%BA%BA%E7%84%A1%E6%B3%95%E6%8E%A5%E5%8F%97%E5%97%8E</id>
        <link href="https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E6%94%B9%E8%AE%8A%E7%8F%BE%E6%B3%81%E7%9A%84%E5%85%AC%E6%8A%95%E7%8F%BE%E6%B3%81%E7%9C%9F%E7%9A%84%E4%BB%A4%E4%BA%BA%E7%84%A1%E6%B3%95%E6%8E%A5%E5%8F%97%E5%97%8E"/>
        <updated>2021-12-17T13:48:27.150Z</updated>
        <summary type="html"><![CDATA[如果你讀了很多資料，還是沒辦法決定公投該怎麼投，那真的不是你的錯。這次至少有三個題目屬於專業領域，我們本來就缺乏作出這些專業判斷所需的經驗。但有些事情是我們有經驗的，那就是現況的生活，而這次的四個公投，都是「改變現況」的公投。現況是萊豬已經開放了一年，請問您是否吃到了萊豬？美國已經吃萊豬 20 年，日本吃了 15 年，台灣人萊牛也吃了 8 年，請問您是否聽聞哪位親朋好友吃了美國牛排後，受到萊劑的…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/709153804274589060.jpg"><figcaption><span>圖片來源：國民黨 Facebook</span></figcaption></figure><p>如果你讀了很多資料，還是沒辦法決定公投該怎麼投，那真的不是你的錯。這次至少有三個題目屬於專業領域，我們本來就缺乏作出這些專業判斷所需的經驗。</p>

<p>但有些事情是我們有經驗的，那就是現況的生活，而這次的四個公投，都是「改變現況」的公投。</p>

<p>現況是萊豬已經開放了一年，請問您是否吃到了萊豬？美國已經吃萊豬 20 年，日本吃了 15 年，台灣人萊牛也吃了 8 年，請問您是否聽聞哪位親朋好友吃了美國牛排後，受到萊劑的毒害？</p>

<p>現況是核四從來未曾啟用，也沒有任何縣市願意接受核廢料。這麼多年來，你是否曾像國民黨議員徐巧芯或是核四公投提案人黃士修一樣，就這麼想用核四的電，寧願「核廢料放我家也沒關係」？</p>

<figure><img src="https://d2a6d2ofes041u.cloudfront.net/resize?norotation=true&amp;url=https%3A%2F%2Fimages.vocus.cc%2F6ca50da7-052c-4dee-8a9d-1995ef8dab48.jpg&amp;width=740&amp;sign=wivjJ1Ut7CHmNkxrGXDfhN8HWaom7B2AV-0Cbk3mJ1k">
<figcaption>核廢料放我家這種不可能也不負責任的說法，國民黨跟黃士修都曾講過。</figcaption>
</figure>

<p>現況是三接已經推到外海而沒有蓋在藻礁上，為了連接陸地的棧橋橋墩只佔了 0.04% 的比例，你是不是連這樣的比例都不能接受，都覺得這樣的現況傷害了你的人性尊嚴？</p>

<p>那為了改變現況，我們要冒的風險呢？與吃萊豬的美國談判 FTA，或是想加入全部會員國都開放萊豬的 CPTPP，不只關心到股市，更與我們每個人未來的工作機會與待遇息息相關，禁止萊豬就是在賭負面影響有多大；核四下的斷層是潛在風險，到時核廢料放哪可能又是一翻公投廝殺，我們準備好一投再投了嗎？把三接遷去台北港還有重重關卡要評估，這一卡又是好幾年的燃煤空污，保護那一小塊藻礁真的有比較環保嗎？</p>

<p>如果現況對我們都沒有造成影響，為什麼我們要去冒這些風險？風險能換到的好處呢？</p>

<p>好處是吃不到本來就沒吃過的萊豬，可以用到你本來就沒用過的核四電力，然後保護了 0.04% 的藻礁，再花錢去挖台北港。</p>

<figure><img src="https://d2a6d2ofes041u.cloudfront.net/resize?norotation=true&amp;url=https%3A%2F%2Fimages.vocus.cc%2F8ee7a62f-5c24-46c8-8e5f-854fd805b806.jpg&amp;width=740&amp;sign=wJPLENQMRPUC5NrmqVo9qp9WWoSFOLJ9hl2s7QlUYFY">
<figcaption>哭天搶地喊著被破壞的藻礁，其實只是橋墩</figcaption>
</figure>

<p>人民是國家的主人，但就像一間公司的持有者，他應該決定的是大方向，應該任命並相信他的執行長，而不是親自到產線去決定我們根本不清楚的細節。代表直接民主的公投，應該是彌補或糾正代議政治嚴重的偏差，而不是好不容易四年一次選出了執行長，卻又馬上要對著她的施政指手畫腳。</p>

<p>那我們花這麼多錢，這麼多時間在幹嘛？老實說，就這次的幾個題目而言，我們就是在陪國民黨前前後後跳探戈而已。我開放萊牛、我要反對萊豬，我封存核四、我也要重啟核四，我填平藻礁、我也要保護藻礁，就是這樣，打我啊笨蛋。</p>

<figure><img src="https://d2a6d2ofes041u.cloudfront.net/resize?norotation=true&amp;url=https%3A%2F%2Fimages.vocus.cc%2Fe7b27007-7c70-4a9d-a330-d10e1865136b.jpg&amp;width=740&amp;sign=Vn6Qmqargzb9J_7JXcJEG88zFRrUp5PMNuHB4FJTZYA">
<figcaption>四個不同意才能打國民黨喔</figcaption>
</figure>

<p>&nbsp;</p>

<p>原刊於<a href="https://vocus.cc/article/61bc702afd89780001c24df8" target="_blank">作者網誌</a></p>]]></content>
        <author>
            <name>林艾德</name>
            <uri>https://www.thestandnews.com/author/sunnyhengchun</uri>
        </author>
        <category label="台灣"/>
        <published>2021-12-17T13:48:27.150Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[如何避免公投變成政治動員的手段]]></title>
        <id>https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E5%A6%82%E4%BD%95%E9%81%BF%E5%85%8D%E5%85%AC%E6%8A%95%E8%AE%8A%E6%88%90%E6%94%BF%E6%B2%BB%E5%8B%95%E5%93%A1%E7%9A%84%E6%89%8B%E6%AE%B5</id>
        <link href="https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E5%A6%82%E4%BD%95%E9%81%BF%E5%85%8D%E5%85%AC%E6%8A%95%E8%AE%8A%E6%88%90%E6%94%BF%E6%B2%BB%E5%8B%95%E5%93%A1%E7%9A%84%E6%89%8B%E6%AE%B5"/>
        <updated>2021-12-17T09:00:40.203Z</updated>
        <summary type="html"><![CDATA[如何避免孩子把哭鬧當成手段？我想大概所有新手爸媽都google過這題，而答案不外乎是別在孩子哭鬧時滿足他的需求。即使在他諸多需求中，有某幾項需求是可以討論的，你也要讓他知道討論必須在冷靜、理性下進行，而不是依靠哭鬧。對，今天還是要講公投，講如何避免公投變成政治動員的手段。作為一個公民，我們有義務去了解公投的內容，透過理性的討論來做出決定，但我也要提醒，真正的理性，要把對象的不理性考慮在內。幾乎所…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/o-BALLOT-BOX-facebook_ifVhS.jpg"><figcaption><span>資料圖片</span></figcaption></figure><p>如何避免孩子把哭鬧當成手段？我想大概所有新手爸媽都google過這題，而答案不外乎是別在孩子哭鬧時滿足他的需求。即使在他諸多需求中，有某幾項需求是可以討論的，你也要讓他知道討論必須在冷靜、理性下進行，而不是依靠哭鬧。</p>

<p>對，今天還是要講公投，講如何避免公投變成政治動員的手段。</p>

<p>作為一個公民，我們有義務去了解公投的內容，透過理性的討論來做出決定，但我也要提醒，真正的理性，要把對象的不理性考慮在內。</p>

<p>幾乎所有人都知道，國民黨就是把公投當成手段，從黨主席朱立倫、網紅羅智強到御用媒體人李艷秋，共同的論述就是「公投內容不重要，重點是要教訓民進黨」，他們不在乎公投會造成什麼實質影響，因為對他們來說，公投不過是政黨對決的延伸，造成的負面效果甚至更好，那又是一個打擊執政黨的機會。</p>

<p>看到許多朋友從各種角度分析公投的利害得失，的確讓人感動，這才是正常民主社會該有的樣子。但與此同時，也有一點難過，因為我看到國民黨就像一個在地上翻滾哭鬧、討著要4項玩具的巨嬰，而公民作為他的父母，卻沒想到要處理孩子濫用情緒的問題，反而在討論同意他買哪個玩具比較好。</p>

<p>萊劑是國民黨開放的，核四的國民黨封存的，把232公頃藻礁鏟平的工業區是國民黨規劃的，我們都知道國民黨現在支持這些題目不是因為在乎，而是在濫用公投，試圖透過這些題目製造混亂，趁亂回到執政崗位。當然，即使在反國民黨政營中，這些題目肯定也各有支持者，這些人一定會覺得：「難道只要國民黨支持，我就不能支持嗎？」但我認為，正是因為你支持，才要更大力地反對國民黨把這些題目拿來政治操作，唯有如此我們才可能回到理性的討論環境。</p>

<p>否則，哪怕是1項，如果你理性討論後的投票結果跟國民黨巨嬰一樣，等於是變相鼓勵他們再次透過政黨動員來濫用民主、濫用公投，這種把公投當成內閣不信任投票的動員方式，必然會使原本屬於公民的公投再次回歸政黨對決，不但失去公投的意義，更是民主的危機。</p>

<p>國民黨目前的策略，就是透過不斷動員的選舉、罷免、公投來痲痹民主，包含公投綁大選這題，也是為了在法定的公投日外，「額外」在大選時也能增加公投投票。這種反覆動員讓人民見不到真正政策的討論，取而代之的，是彷彿永無止盡的政黨對決。</p>

<p>這種厭倦首先會造成投票率的降低，而投票率越低，對固樁能力好的政黨越有利。其次，這會造成民粹政治人物的崛起，因為民粹政治人物的話術跟空頭支票，最能召喚出那些不關心政治的選民，再配合上原本的政黨鐵票，這樣的政治人物選舉實力是最強的。</p>

<p>韓國瑜現象就是政黨強大的固樁加上他個人煽動「非政治選民」的能力，使國民黨在全世界這波民粹浪潮中初嚐甜頭。所幸，台灣的貧富差距尚未急遽擴大，經濟也在疫情影響中逆勢成長，這都不利於民粹主義發展，但不幸的是，台灣有國民黨在刻意製造紛亂，以便讓下一個韓國瑜崛起。</p>

<p>我的想法是，這次我們的投票不只關係到政策，也關係到未來台灣的民主環境，關係到未來的公投能不能回歸理性討論？還是國民黨會繼續不斷地政治動員下去，直到台灣成為民粹主義者的溫床？希望這次，我們展現出的民意可以徹底制止這股歪風，不要再讓國民黨有錯誤解讀人民意志的機會。</p>

<p>&nbsp;</p>

<p><a href="https://vocus.cc/article/61bae024fd897800016a8015" target="_blank">作者 vocus</a></p>]]></content>
        <author>
            <name>林艾德</name>
            <uri>https://www.thestandnews.com/author/sunnyhengchun</uri>
        </author>
        <category label="台灣"/>
        <published>2021-12-16T10:50:23.350Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[又又又又又蔡英文論文一擊斃命]]></title>
        <id>https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E5%8F%88%E5%8F%88%E5%8F%88%E5%8F%88%E5%8F%88%E8%94%A1%E8%8B%B1%E6%96%87%E8%AB%96%E6%96%87%E4%B8%80%E6%93%8A%E6%96%83%E5%91%BD</id>
        <link href="https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E5%8F%88%E5%8F%88%E5%8F%88%E5%8F%88%E5%8F%88%E8%94%A1%E8%8B%B1%E6%96%87%E8%AB%96%E6%96%87%E4%B8%80%E6%93%8A%E6%96%83%E5%91%BD"/>
        <updated>2021-11-30T13:50:32.802Z</updated>
        <summary type="html"><![CDATA[昨晚回家，家人喜孜孜地（我們是正藍家庭）告訴我，蔡英文被證實沒有論文了，有誠信問題的總統應該要下台。這已經是不知道第幾次聽到所謂「一擊斃命」的證據了，每一次，最後被證實的都只有這一幫人的無恥。老實說，還沒看證據時就已經覺得沒有可信度，但這樣的言論來自家人還是讓我震驚。蔡英文的論文早已可以公開下載，我原本以為，只有最沒有資訊辨識能力、最容易被煽動的人才會繼續接受這種假訊息，但我們家，至少我自認是有…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/109529428_YmE3w_j6BYoO1.png"><figcaption><span>蔡英文（資料圖片，來源：蔡英文  Facebook）</span></figcaption></figure><p>昨晚回家，家人喜孜孜地（我們是正藍家庭）告訴我，蔡英文被證實沒有論文了，有誠信問題的總統應該要下台。</p>

<p><a href="https://tw.news.yahoo.com/lse%E5%90%A6%E8%AA%8D%E6%8C%81%E6%9C%89%E8%94%A1%E8%8B%B1%E6%96%87%E5%8D%9A%E5%A3%AB%E5%8F%A3%E8%A9%A6%E5%A0%B1%E5%91%8A-%E8%B3%80%E5%BE%B7%E8%8A%AC%E5%97%86%E4%B8%8B%E5%8F%B0%EF%BC%81%E5%BD%AD%E6%96%87%E6%AD%A3%E5%91%8A%E5%85%B1%E7%8A%AF-053411367.html" target="_blank">這已經是不知道第幾次聽到所謂「一擊斃命」的證據了</a>，每一次，最後被證實的都只有這一幫人的無恥。老實說，還沒看證據時就已經覺得沒有可信度，但這樣的言論來自家人還是讓我震驚。蔡英文的論文早已可以<a href="https://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi?o=dnclcdr&amp;s=id=%22073LSE00194001%22.&amp;searchmode=basic#XXX" target="_blank">公開下載</a>，我原本以為，只有最沒有資訊辨識能力、最容易被煽動的人才會繼續接受這種假訊息，但我們家，至少我自認是有那麼一點文化的。</p>

<p>看了那兩封所謂的證據，悲哀又深了一層。對方清清楚楚的在信上寫著無論是 LSE 或是倫敦大學都確認過蔡英文的學位與論文，唯一的問題是蔡英文的指導教授沒有繳回屬於他的影本給 LSE 圖書館，結果這群人只把重點畫在「LSE 圖書館沒有蔡英文的論文」。</p>

<p>再更早一點，今年 9 月<a href="https://informationrights.decisions.tribunals.gov.uk/DBFiles/Decision/i2943/032%20200921%20Richardson%20EA%202020%200212%20p.pdf" target="_blank">英國法院的判決文</a>中，就已經檢視及確認過倫敦大學持有蔡英文論文口試委員名單及他們簽署論文口試時間的文件，並認定倫敦大學提供的資訊足以證實學位的真實性。</p>

<p>結果，這些人又去找 LSE 要相同的文件，LSE 當然回答說沒有，<strong>因為這些文件就在倫敦大學而且法院已經認證過</strong>，為什麼要一直找沒有儲存這些文件的 LSE 要？對方甚至寫明了不會再處理你們的無理取鬧。</p>

<p>只能說，英國人真的高估了這批人的羞恥心。他們就是故意要找 LSE 要，才能把紅色底線畫在「LSE 沒有蔡英文口試文件」。</p>

<p><iframe allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowfullscreen="true" frameborder="0" height="673" scrolling="no" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FKaohsiung.NewTide%2Fposts%2F481994190019695&amp;show_text=true&amp;width=500" style="border:none;overflow:hidden" width="500"></iframe></p>

<p>讓我覺得最不要臉的，是這些資訊都明明白白寫在他們提供的文件上，結果他們卻能睜眼說瞎話，彷彿英文是一種台灣人無法閱讀的語言。又或者說，他們很清楚媒體只會照著他們的話寫，而他們的目標客群只要看到標題就會興奮得起舞，根本不會去理會所謂「證據」的原文完全就是相反意思。</p>

<p>如果昨晚聽家人說這件事的感覺是驚訝，那讀完「證據」之後，我的感覺就是丟臉，真的好丟臉。當然不是為我的家人丟臉，而是為彭文正這批人感到見笑。</p>

<p>上一代不像我們能輕易擁有知識，他們絕大部分就像我的家人一樣，還未成年就開始工作，即使稍微有點能力後，也總是把最好的資源都留給我們，這是他們偉大之處。但彭文正這些人卻玩弄著他們的犧牲，利用他們外語能力的不足一路搬弄是非，臉皮厚到直接提供一份完全相反意思的文件，彷彿當面在嘲笑他們看不懂。</p>

<p>為了什麼呢？</p>

<p>北檢去年就找到了蔡英文論文案的起源，並起訴了統促黨的何建華。她涉嫌在總統大選期間接受安徽財經大學副教授黃信瑜指導，黃男於 2019 年 11 月 9 日由大陸中國農業銀行匯款 3 萬元人民幣到何建華廈門銀行帳戶，<strong>何女領取現款後，黃男提供《拒絕假博士，人民要真相》新聞稿給何女發送，試圖影響台灣選情。</strong></p>

<p>總統大選前玩一次，現在要公投了又要玩一次，明知道騙不了年輕人，但擺明了就是要騙我們的上一代、騙那些為台灣努力奉獻的基層。究竟多少像我們一樣的家庭為了這種荒唐事起無謂的紛爭？白紙黑字的東西，都能把黑的說成白的，良心真的不會痛嗎？</p>

<p>&nbsp;</p>

<p>原刊於<a href="https://vocus.cc/article/61a58ba8fd8978000158d736" target="_blank">作者網誌</a></p>]]></content>
        <author>
            <name>林艾德</name>
            <uri>https://www.thestandnews.com/author/sunnyhengchun</uri>
        </author>
        <category label="台灣"/>
        <published>2021-11-30T13:47:37.385Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[三分鐘盪鞦韆計時器的道德問題]]></title>
        <id>https://www.thestandnews.com/society/%E4%B8%89%E5%88%86%E9%90%98%E7%9B%AA%E9%9E%A6%E9%9F%86%E8%A8%88%E6%99%82%E5%99%A8%E7%9A%84%E9%81%93%E5%BE%B7%E5%95%8F%E9%A1%8C</id>
        <link href="https://www.thestandnews.com/society/%E4%B8%89%E5%88%86%E9%90%98%E7%9B%AA%E9%9E%A6%E9%9F%86%E8%A8%88%E6%99%82%E5%99%A8%E7%9A%84%E9%81%93%E5%BE%B7%E5%95%8F%E9%A1%8C"/>
        <updated>2021-10-12T14:18:13.617Z</updated>
        <summary type="html"><![CDATA[根據台北市公園處的說法，在大安森林、青年、南港、前港、碧湖、花博等六個公園的鞦韆旁設置三分鐘計時器，是為了讓孩子領會遊戲中的分享的精神，輔導孩子們養成排隊輪流玩的習慣。這又是一個規訓侵入道德領域的例子，當人們以為規訓可以養成道德時，常常會適得其反。以這個例子來說，當其他縣市的孩子來到台北，「三分鐘計時器」只不過在他們習慣的分享原則上給予客觀標準，但如果你的孩子是從小就在台北這六個公園玩，反而會因…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/20211010001521.jpg"><figcaption><span>圖片來源：台北市政府公園處</span></figcaption></figure><p>根據台北市公園處的說法，<a href="https://tw.appledaily.com/life/20211010/KGFPWI2OLNB6VPTVBXC5ECD6PQ/" target="_blank">在大安森林、青年、南港、前港、碧湖、花博等六個公園的鞦韆旁設置三分鐘計時器</a>，是為了讓孩子領會遊戲中的分享的精神，輔導孩子們養成排隊輪流玩的習慣。</p>

<p>這又是一個規訓侵入道德領域的例子，當人們以為規訓可以養成道德時，常常會適得其反。</p>

<p>以這個例子來說，當其他縣市的孩子來到台北，「三分鐘計時器」只不過在他們習慣的分享原則上給予客觀標準，但如果你的孩子是從小就在台北這六個公園玩，反而會因此錯失學習分享、增進同理心的機會。對在規訓中成長的他而言，把鞦韆交出給下一個孩子並不是為了分享、也不是因為他能體會別人的心情，僅僅是因為「三分鐘到了」。</p>

<p>所以這些孩子到外縣市會發生什麼事？比起其他孩子，他們有較高的可能會佔著鞦韆不放，因為在這裡，沒有任何規矩要求他離開。甚至，即使在台北，他們也有較大的機會霸佔共享的玩具，畢竟玩具可沒有三分鐘的規定。</p>

<p>民主社會中，道德勸戒與強制規訓之間的取捨一直都是重中之重。當原本屬於公民道德領域的善行，變成一種必須遵守的規定時，那人們從事這項行為的動機，也會從無私的共善，變成貪求獎勵或是畏懼懲罰。於是，我們需要越來越多的<strong>監視與枷鎖</strong>，只為了維持那個原本善良的社會，威權領導人也可以更輕易地以維持秩序之名來消滅反對的聲音，完全違反了民主的初衷。</p>

<p>當然，盪鞦韆只是現代孩子千百種遊戲中的一項，討論這件事並不是要滑坡成「柯文哲教壞小孩」，而是提醒若連盪鞦韆這種事情都要訂出規矩，乃至於鉅細彌遺地在生活中制定各種規範，那我們的孩子會失去很多學習尊重以及與他人凝聚共識的機會，而<strong>這正是經歷世界最長戒嚴的台灣人所最欠缺的能力，這樣的匱乏，不該再透過錯誤的教育方式毒害我們的下一代。</strong></p>

<p>&nbsp;</p>

<p>原刊於<a href="https://vocus.cc/article/61651890fd89780001349660" target="_blank">作者網誌</a> / <a href="https://www.facebook.com/sunnyhengchun/posts/4379611785421888" target="_blank">Facebook</a></p>]]></content>
        <author>
            <name>林艾德</name>
            <uri>https://www.thestandnews.com/author/sunnyhengchun</uri>
        </author>
        <category label="社會"/>
        <published>2021-10-12T14:18:13.617Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[風起了，我們以後一定會自由]]></title>
        <id>https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E9%A2%A8%E8%B5%B7%E4%BA%86-%E6%88%91%E5%80%91%E4%BB%A5%E5%BE%8C%E4%B8%80%E5%AE%9A%E6%9C%83%E8%87%AA%E7%94%B1</id>
        <link href="https://www.thestandnews.com/%E5%8F%B0%E7%81%A3/%E9%A2%A8%E8%B5%B7%E4%BA%86-%E6%88%91%E5%80%91%E4%BB%A5%E5%BE%8C%E4%B8%80%E5%AE%9A%E6%9C%83%E8%87%AA%E7%94%B1"/>
        <updated>2021-06-07T03:03:00.000Z</updated>
        <summary type="html"><![CDATA[良寬禪師是19世紀初期日本著名的詩人及書法家。相傳某天他遇到一群正放著風箏、卻等不到好風的孩童，其中一個孩子見到良寬，馬上跑了過來拜託他寫幾個字，良寬便在風箏上題了「天上大風」，意喻期待孩子們努力放高的風箏能遇上大風湧動，自由高飛。得知了日本將在明天送來120萬劑疫苗的消息，又看到日台交流協會在臉書寫下：「桃太郎與炭治郎在長期跟魔鬼搏鬥的辛苦日子裡，腦海中說不定曾閃過『乾脆加入鬼聯盟還比較不會這…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/190250767_3993257804057290_7561021800831775219_n_jpt4K.jpg"><figcaption><span>良寬禪師作品「天上大風」</span></figcaption></figure><p>良寬禪師是19世紀初期日本著名的詩人及書法家。相傳某天他遇到一群正放著風箏、卻等不到好風的孩童，其中一個孩子見到良寬，馬上跑了過來拜託他寫幾個字，良寬便在風箏上題了「天上大風」，意喻期待孩子們努力放高的風箏能遇上大風湧動，自由高飛。</p>
<p>得知了日本將在明天送來120萬劑疫苗的消息，又看到<a href="https://www.facebook.com/JiaoliuxiehuiTPEculture/posts/3978225908931739" target="_blank">日台交流協會在臉書</a>寫下：「桃太郎與炭治郎在長期跟魔鬼搏鬥的辛苦日子裡，腦海中說不定曾閃過『乾脆加入鬼聯盟還比較不會這麼累！』這樣的念頭。即便如此，他們努力抵抗誘惑，咬緊牙關堅持到底，最後終於和夥伴們迎向了幸福的結局。」心裡馬上想起這個故事。</p>
<p>其實我們都知道，在現實世界裡，更多時候好人總是吃虧，甚少迎向幸福的結局。但我們依舊喜歡好人有好報的童話，只要有機會幫助別人，許多台灣人仍會傾囊相助，這不是因為我們真的相信能得到什麼，而是長期處在困境中、早已習慣被打壓的我們，仍期盼世界能看見我們的好、知道我們值得更好的對待。</p>
<p>就像那群拼命向前奔跑的孩子。我們相信只要不放棄地一直跑，總有一天，我們的風箏會遇上大風，自由地飛翔。</p>
<p>今天晚上，似乎感受到北方吹來的徐徐微風。雖然疫情隔開了你我，但還是好想跟多年一起奮鬥的夥伴們說，風起了，我們以後一定會自由。</p>
<p>作者<a href="https://www.facebook.com/photo/?fbid=3993257800723957&amp;set=a.338556119527495" target="_blank"> Facebook</a></p>
<p>(標題為編輯所擬)</p>
<p>&nbsp;</p>
]]></content>
        <author>
            <name>林艾德</name>
            <uri>https://www.thestandnews.com/author/sunnyhengchun</uri>
        </author>
        <category label="台灣"/>
        <published>2021-06-07T03:02:40.000Z</published>
    </entry>
</feed>