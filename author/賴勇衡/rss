<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>https://www.thestandnews.com/author/賴勇衡/rss</id>
    <title>立場新聞 Stand News - 賴勇衡</title>
    <updated>2021-12-07T09:58:02.245Z</updated>
    <generator>https://github.com/jpmonette/feed</generator>
    <link rel="alternate" href="https://www.thestandnews.com/author/賴勇衡"/>
    <subtitle>《立場新聞》編採獨立自主，不受任何贊助人、財團、權力機構及黨派左右。我們以非牟利原則營運，所有經營盈餘和贊助，只會用於傳媒事業。</subtitle>
    <rights>© 2021 立場新聞. All rights reserved.</rights>
    <entry>
        <title type="html"><![CDATA[《這不是一場葬禮》— 唔好蝦阿婆]]></title>
        <id>https://www.thestandnews.com/culture/%E9%80%99%E4%B8%8D%E6%98%AF%E4%B8%80%E5%A0%B4%E8%91%AC%E7%A6%AE-%E5%94%94%E5%A5%BD%E8%9D%A6%E9%98%BF%E5%A9%86</id>
        <link href="https://www.thestandnews.com/culture/%E9%80%99%E4%B8%8D%E6%98%AF%E4%B8%80%E5%A0%B4%E8%91%AC%E7%A6%AE-%E5%94%94%E5%A5%BD%E8%9D%A6%E9%98%BF%E5%A9%86"/>
        <updated>2021-10-22T11:07:04.056Z</updated>
        <summary type="html"><![CDATA[若果信仰可以賦予人生意義，反過來說，失去人生意義也可使人失去信仰。《這不是一場葬禮》的主角是一個年邁寡婦，接連遭受喪親之痛與權勢逼迫，彷彿一切都失去意義，只有虛無，那麼原片名 This is Not a Burial, It’s a Resurrection 中的「復活」所指為何？常言道信念給人力量，但我們可否從另一個角度想：宗教信仰怎樣失去力量？（劇透）在非洲有一個國中之國萊索托（Lesoth…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/Eq4Hha537UwAAPpCj.jpg"><figcaption><span>電影《這不是一場葬禮》（This is Not a Burial, It’s a Resurrection）劇照</span></figcaption></figure><p>若果信仰可以賦予人生意義，反過來說，失去人生意義也可使人失去信仰。《這不是一場葬禮》的主角是一個年邁寡婦，接連遭受喪親之痛與權勢逼迫，彷彿一切都失去意義，只有虛無，那麼原片名 <em>This is Not a Burial, It’s a Resurrection</em> 中的「復活」所指為何？常言道信念給人力量，但我們可否從另一個角度想：宗教信仰怎樣失去力量？</p>

<p>（劇透）在非洲有一個國中之國萊索托（Lesotho），國境完全被南非所包圍，政治和經濟上都很倚賴南非。這個國家曾為英國殖民地，絕大部份國民信奉基督宗教。住在山村「拿撒勒」的老寡婦 Mantoa 等待在南非當礦工的兒子回家過聖誕，卻得到工殤惡耗。萬念俱灰的她只求死後在村中墓地與祖先同葬，結果連這卑微的願望也幻滅，因為水壩工程將會淹沒這個地方。雖然阿婆一度團結了全村上下，向有關當局爭取不遷不拆，對方卻以殺手放火的手段威嚇。最後在村民遷走的行列中，阿婆倏地回頭、脫衣、坦然迎向強拆者……</p>

<p><strong>阿婆之苦 vs 約伯之難</strong></p>

<p>苦難難受，面對苦難的問題沒答案亦難受，但面對別人廉價的「解答」可能更難受。阿婆不是像約伯那樣的「義人」，只是尋常人家，在兒子身上體驗到上帝的恩慈，也因此在喪子之後失去了信心。曾經喪偶的神父表示感同身受，嘗試見證徹底向上帝主權順服的平安。但這些「正確的教導」對阿婆來說沒有意義。她沒有得著安慰，或許上帝就是不安慰。</p>

<p>壞事一浪接一浪，沒有最絕望，只有更絕望。阿婆一直不肯換掉喪服，令村民開始覺得她走火入魔，但這其實是一個預兆 — 這個村莊也在步向死亡。本來團結村民保衛村莊的行動令阿婆一度從死蔭幽谷裡走出來，卻旋即被邪惡的一方逼回去。她跪倒在自己掘的墓穴中，無語問蒼天。</p>

<p><strong>向誰順服？哪個權柄？</strong></p>

<p>對阿婆來說，神父的分享和教堂的敬拜再沒有意義，或許不只是因為她遭受苦難，而是因為這個教會見證出來的只有一片虛無。這個神父的角色並不代表世界上所有的神僕，但他是村內唯一的宗教代表兼知識份子，整條村都看他活出怎樣的見證。神父是個典型的老好人，但他的順服，結果是向以「經濟發展」之名輾壓百姓的順服；他的軟弱，只能映照出世俗權勢的大能。這個逼遷的情節來自現實，萊索托政府配合南非及世界銀行的水利工程計劃，令很多人失去家園及賴以維生的天然資源。所以這裡拋出的問題，不只是那個虛構的神父角色要面對，而是現實信徒都可能會面對的問題：若要在困境中要順服，在生命中見證出來的，究竟是向哪一個主權順服？</p>

<p>導演 Lemohang Jeremiah Mosese 似乎亦藉著對「發展」的質疑，反思基督教、殖民與現代化的關係。宗教與世俗權柄的共謀、社群傳統之消逝並非新鮮事：這條村莊本來叫「悲傷草原」，是歷代祖先安葬之地；在十九世紀，法國傳教士來到，是歐洲殖民者的先頭部隊，把這裡改名為「拿撒勒」。這次水利工程要把村民的祖墳連根拔起，也翻起了阿婆世代相傳的族群記憶。這裡帶出了有關文化的二律背反：傳統價值應薪火相傳，但文化總是在跨界交流與變動之中構成。問題是，諸事變與不變是誰說了算？變或不變，對人的生命與心靈有怎樣的影響？</p>

<p><strong>生人復活的盼望</strong></p>

<p>阿婆的經歷其實是一個被轉述的故事，出自一個「講故佬」之口。他身處於一個破落的酒吧，吹奏著當地傳統樂器 Lesiba，講述「這個地方」的過去，暗示「發展」的美好圖景往往是慘淡收場，如今教堂鐘聲與眾人的靈魂皆已浸沒在水底之下。那麼之後電影展現的一切，是否意味著米已成炊、烏雲蓋天，所有意義和信念皆被現實的黑手捏碎？</p>

<p>電影的結局正是抗衡著這看似宿命論的開場。阿婆無法改變死亡和遭受迫遷的事實，但她選擇了不妥協，卻不動武；高舉雙手，卻非投降。導演沒有描寫她赤身步向壓迫者的結果如何 — 是生是死 — 他突然把鏡頭從老人的背影轉向 180 度，映著一個見證著這重要時刻的小女孩：「她看到的是復活，卻非死而復生，而是活人重生。」</p>

<p>阿婆的背影令人想起卡繆在《反抗者》中這樣回應荒謬無意義的世界：「我反抗，故我存在。」即使無法改變不公義的現實，仍然要對之說「不」，因為這樣同時也就是對某些價值說「是」，而非墮入虛無。另一方面，導演對「重生」之詮釋亦指向了新的盼望：並非僅僅寄望死後復活，今生便俯服於世界，而是於此世見證生命更新。</p>

<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/kBAfSS7ux3o" title="YouTube video player" width="560"></iframe></p>

<p>&nbsp;</p>

<p>（原載於<a href="https://christiantimes.org.hk/Common/Reader/News/ShowNews.jsp?Nid=166937&amp;Pid=2&amp;Version=1781&amp;Cid=1156&amp;Charset=big5_hkscs" target="_blank">《時代論壇》1781 期</a>）</p>]]></content>
        <author>
            <name>賴勇衡</name>
            <uri>https://www.thestandnews.com/author/%E8%B3%B4%E5%8B%87%E8%A1%A1</uri>
        </author>
        <category label="文化"/>
        <published>2021-10-22T11:03:11.565Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[《持家小木蘭》和《掀起面紗的少女》— 細路女都唔放過的塔利班]]></title>
        <id>https://www.thestandnews.com/culture/%E6%8C%81%E5%AE%B6%E5%B0%8F%E6%9C%A8%E8%98%AD%E5%92%8C%E6%8E%80%E8%B5%B7%E9%9D%A2%E7%B4%97%E7%9A%84%E5%B0%91%E5%A5%B3-%E7%B4%B0%E8%B7%AF%E5%A5%B3%E9%83%BD%E5%94%94%E6%94%BE%E9%81%8E%E7%9A%84%E5%A1%94%E5%88%A9%E7%8F%AD</id>
        <link href="https://www.thestandnews.com/culture/%E6%8C%81%E5%AE%B6%E5%B0%8F%E6%9C%A8%E8%98%AD%E5%92%8C%E6%8E%80%E8%B5%B7%E9%9D%A2%E7%B4%97%E7%9A%84%E5%B0%91%E5%A5%B3-%E7%B4%B0%E8%B7%AF%E5%A5%B3%E9%83%BD%E5%94%94%E6%94%BE%E9%81%8E%E7%9A%84%E5%A1%94%E5%88%A9%E7%8F%AD"/>
        <updated>2021-09-17T09:29:14.818Z</updated>
        <summary type="html"><![CDATA[阿富汗最近再次「變天」，原教旨主義者塔利班重新掌權。其中一項最令人擔憂的，是阿富汗女性的人權狀況會大倒退。有不少以當代阿富汗作背景的電影皆以女性為首要主角，呈現她們的生存困境，包括《掀起面紗的少女》（2003）和動畫電影《持家小木蘭》（2017）。在塔利班演繹的伊斯蘭教法之下，全體婦女處於弱勢，未成年的女孩更是弱中之弱。對製作者和觀眾來說，這是一種訴諸同情心與正義感的情感策略 —「細路女都唔放過…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/563245423456254123742534.jpg"><figcaption><span>《掀起面紗的少女》（Osama，圖左）、《持家小木蘭》（The Breadwinner，圖右）宣傳照</span></figcaption></figure><p>阿富汗最近再次「變天」，原教旨主義者塔利班重新掌權。其中一項最令人擔憂的，是阿富汗女性的人權狀況會大倒退。有不少以當代阿富汗作背景的電影皆以女性為首要主角，呈現她們的生存困境，包括《掀起面紗的少女》（2003）和動畫電影《持家小木蘭》（2017）。在塔利班演繹的伊斯蘭教法之下，全體婦女處於弱勢，未成年的女孩更是弱中之弱。對製作者和觀眾來說，這是一種訴諸同情心與正義感的情感策略 —「細路女都唔放過？」此問預設了強者會對最可憐弱勢者網開一面，但從欺凌者的角度而言，正是「細路女先唔放過」。</p>

<p>（下文劇透）這兩齣戲都再現了女性如何在 1990 年代塔利班治下舉步維艱：不能接受教育和工作，外出須由家中男性陪同，且要套上遮蓋全身的罩袍（Burqa），連眼睛也要擋住。但男性即使享有獨自外出和工作的權利，也不代表逍遙自在。男孩會被徵召受訓為塔利班的戰士，他們的父輩卻可能因戰爭而喪命或失去工作能力；不願合作的人會被拘禁甚至處決。因此有些家庭僅留下被禁止外出的女性，若無親戚接濟，猶如要在家中等死。鋌而走險外出，被宗教警察抓到，又會受罰；於是這兩個故事都出現了小女孩像花木蘭般裝扮成男孩、外出打工養家的橋段。但在嚴苛到荒謬的極權治下，這種易裝把戲真能偷天換日？</p>

<p><strong>《持家小木蘭》（The Breadwinner）</strong>改編自 Deborah Ellis 創作的兒童小說，監製是著名女星兼聯合國難民署親善大使安祖蓮娜祖莉，主要創作者皆來自西方國家。所以這齣動畫充份表達出西方主流的普世價值觀，除了人權和自由，還突顯女性角色的主動性；相對而言，男性角色大都是反派（如塔利班）或相對被動的（如主角被囚的父親）。雖然故事題材沉重，涉及貧窮、戰亂和暴政，視覺風格仍是線條乾淨、色彩悅目；很多特寫畫面強調女主角 Parvana 的綠色大眼睛，配合最後美軍聯盟「解放」、一家團圓的結局，是個充滿希望的兒童故事。</p>

<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/SnpBc8YvGpk" title="YouTube video player" width="560"></iframe></p>

<p><strong>《掀起面紗的少女》（Osama）</strong>則是阿富汗導演 Siddiq Barmak 的作品，雖有西方國家合製，阿富汗也是產地之一，感覺更似現身說法，拍出一片荒涼又殘酷的現實。女主角鋌而走險，終日擔驚受怕。她的容貌如 Parvana 同樣標緻，卻成了破綻，結果少女初熟，紙包不住火。結局演繹塔利班式「依法治國」，死刑有不同方式，任由法官一聲裁決；少女被嫁給老人作妻，已是「神恩浩蕩」的特赦，直送老人後宮，連新娘親屬也不用通知。重門深鎖，老人拿起一串鎖，再叫少女選一個，盡顯所謂「有得揀」的荒謬。</p>

<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/54sKYNvd3CY" title="YouTube video player" width="560"></iframe></p>

<p>比較起來，《持》像包著糖衣的藥丸，在西方國家發行時還附有教材套，是滿瀉的正能量心靈雞湯；《掀》卻揭開殘酷現實的不同面相，前路限於死亡與為奴之間，其中的「可能性」僅在於不同的死亡方式，或讓奴隸選擇自己的枷鎖。這看來也就是希望與絕望的分別。</p>

<p>然而這兩齣電影還有另一共通主題：「故事」，或者說「講故事的力量」。講故事是教育的方式，像 Parvana 的父親就是透過故事教育。但這本身不一定是正面的，因為有些人可以透過故事灌輸某些意識形態，或曰「洗腦」，難怪也有人質疑《持》是宣傳西方價值觀的工具，例如批評影片中對塔利班的描寫流於刻板印象。有趣的是，這齣動畫最後走向大團圓，全靠設計一個「善良的塔利班」角色捨身相助，劇情異於原著。這種編排更像是鞏固還是挑戰人們對塔利班的既有印象？反過來說，把希望放在塔利班悔改，是否太過「大愛」而天真，在現實中結果就會滿盤落索？這個問題，要交給每一個人問自己的信仰是甚麼。</p>

<p>但《持》有關「講故事」的關鍵情節在於 Parvana 如何面對喪親的哀傷，與主情節交替敘述，互為表裡。這本來是她用來哄幼弟的隨興創作，講述一個小男孩集齊三件法寶，去找惡霸「大象王」討回被奪去的農作物種子，不然全村會餓死。 小男孩就是&nbsp; Parvana 的哥哥 Sulayman，早年被地雷炸死，如同現實中很多阿富汗孩子的遭遇。最後他讓「大象王」回轉並交回種子，就是靠講述自己意外身亡的故事。在小女孩的想像中，哥哥與自己的死亡達成諒解。Sulayman 的死亡是無可改變的事實，但他卻在虛構的故事中繼續冒險，帶來安慰。</p>

<p>《掀》有關「講故事」的元素在結構上沒有那麼重要，只是在初段點題，由少女主角的外祖母講述「男孩穿過彩虹橋變成女孩，再穿過來變回男孩」的故事，再讓她假裝成男孩子外出打工。這一點也顯示出兩齣戲對女主角的不同處理：Parvana 較主動，自己決定剪髮變裝；《掀》的少女則較被動，是被長者逼著冒險。但戲裡有一個畫龍點睛的意象，就是跳繩。她女扮男裝打工時，在小店的內房跳過一次，後來被困在監牢，以及最後「被洞房」之後，跳繩卻是一個想像。有些觀眾認為，跳繩就像經過彩虹橋，讓她變成男孩，象徵她渴望像真正的小男孩一般玩耍。這想像就是她最後的、僅餘的，給自己的心靈自由。</p>

<blockquote>
<p><strong>延伸參考：</strong></p>

<p><strong>1. Hana Makhmalbaf 導：《佛在恥辱中倒塌》（Buddha Collapsed Out of Shame），伊朗，2007。（電影）</strong></p>

<p>此片同樣有關女孩在阿富汗的困境，雖然在美軍「解放」之後，女性可接受教育，但實行起來卻不容易。但戲中那群熱衷玩戰爭遊戲的男孩，立場可轉，欺凌不變，才是真正令人心寒。</p>

<p><strong>2. Pietra Brettkelly 導：《膠卷救援任務》（A Flickering Truth），新西蘭，2015。（電影）</strong></p>

<p>塔利班嚴禁攝影造像，更不容許電影。他們首次掌權後燒毁大量阿富汗昔日電影菲林，在他們被美軍趕走後，卻還有一些殘留影像重見天日，但修復之路艱鉅。不料塔利理今日重來，才搶救回來沒多少年的殘卷，能否再次逃過厄運？</p>

<p><strong>3. Girish Malik 導：《球勝難民營》（Torbaaz），印度，2020。（電影）</strong></p>

<p>較接近主流商業套路的電影，男主角用板球來教育阿富汗難民兒童，希望能讓幾個被塔利班操控當人肉炸彈的孩子回轉。在兩相拉扯之下，扭橋扭出令人扼腕的結局。</p>
</blockquote>

<p>&nbsp;</p>

<p>原載於<a href="https://christiantimes.org.hk/Common/Reader/News/ShowNews.jsp?Nid=166653&amp;Pid=102&amp;Version=0&amp;Cid=2139&amp;Charset=big5_hkscs" target="_blank">《時代論壇》1776 期</a></p>]]></content>
        <author>
            <name>賴勇衡</name>
            <uri>https://www.thestandnews.com/author/%E8%B3%B4%E5%8B%87%E8%A1%A1</uri>
        </author>
        <category label="文化"/>
        <published>2021-09-17T09:11:32.980Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[《別當我公主》中的安蒂岡妮]]></title>
        <id>https://www.thestandnews.com/international/%E5%88%A5%E7%95%B6%E6%88%91%E5%85%AC%E4%B8%BB%E4%B8%AD%E7%9A%84%E5%AE%89%E8%92%82%E5%B2%A1%E5%A6%AE</id>
        <link href="https://www.thestandnews.com/international/%E5%88%A5%E7%95%B6%E6%88%91%E5%85%AC%E4%B8%BB%E4%B8%AD%E7%9A%84%E5%AE%89%E8%92%82%E5%B2%A1%E5%A6%AE"/>
        <updated>2021-09-17T09:29:59.252Z</updated>
        <summary type="html"><![CDATA[痛失親人，為其安葬，天經地義。但國王下令禁止，因為死者是「逆賊」。死者的妹妹堅持埋葬哀悼，遭下令處死。這是希臘神話裡安蒂岡妮（Antigone）的故事，也令人想起古今中外對悼念死者所下的政治禁令。安蒂岡妮也象徵了父權體制下的女性自主行動，為《別當我公主》中一群來自敘利亞的女性難民提供了療癒與表達的渠道。敘利亞內戰多年，無數人死傷流離。有關敘利亞難民的紀錄片不少，但《別當我公主》（We Are N…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/1871646218764.jpg"><figcaption><span>紀錄片《別當我公主》（We Are Not Princesses）圖片</span></figcaption></figure><p>痛失親人，為其安葬，天經地義。但國王下令禁止，因為死者是「逆賊」。死者的妹妹堅持埋葬哀悼，遭下令處死。這是希臘神話裡安蒂岡妮（Antigone）的故事，也令人想起古今中外對悼念死者所下的政治禁令。安蒂岡妮也象徵了父權體制下的女性自主行動，為《別當我公主》中一群來自敘利亞的女性難民提供了療癒與表達的渠道。</p>

<p>敘利亞內戰多年，無數人死傷流離。有關敘利亞難民的紀錄片不少，但《別當我公主》（We Are Not Princesses）並不純粹「賣慘」，在傳達傷痛之外，也讓一些女性難民在參加戲劇治療活動的過程中，探索自己的聲音，以藝術創作表現出來。類似的主題也見於《秘洞醫院》（The Cave）及《致莎瑪︰敘利亞家書》（For Sama）這兩齣紀錄片，講述兩位敘利亞女性面對戰爭，分別在醫生和記者的崗位盡其所能。</p>

<p>《別》中幾位女性難民沒有專業人士的身份，仍然透過藝術尋求改變。她們面對的不單是戰亂，還有壓在頭上多年的男尊女卑傳統。她們走難到黎巴嫩，雖然家破甚至人亡，但也得到了忠誠地表達自我的空間。在戲劇工作坊中，她們把自己的經歷和安蒂岡妮的故事對照，並對其中的角色各自詮釋與評價。故事中兩個王子為爭奪權力而兄弟相殘，讓她們聯想到祖國內戰；有些人經歷喪子之痛，其情意也共通於安蒂岡妮捨命葬兄的「勇毅忠誠」。</p>

<p>戲劇既可療傷，也可充權。這些女性素來在高壓的傳統氣氛之下過活：不准露出頭髮、不准沒戴臉紗外出、不要大笑、不要抽煙……現在流落異國，她們反而有空間放下那些枷鎖，在鏡頭面前吞雲吐霧、談笑風生。但觀眾不知道她們是否少數例外，因為紀錄片的鏡頭本身就是篩選的機制；戲劇工作坊當中，只有那些願意又能夠露面發聲的參加者才能成為這齣紀錄片的「主角」。</p>

<p>有一些女性難民因為家人反對，無法露面，導演便用動畫來講述她們的心聲。這或會令一些觀眾疑惑，因為紀錄片「理應」是客觀紀實的，《別》中的動畫卻運用了不少非現實元素。一方面，動畫可被視為折衷的手法，讓無法出鏡的人參與；另一方面，這種方式也便利於記述一些主觀層面的真實，即人的情感、渴望和想像。這正好呼應著這群女性參演《安蒂岡妮》這集體行動 — 這是紀錄片的核心事件，本身就是以虛構敘事來帶動參加者，讓她們交流真實的經歷和感想。</p>

<div><figure><img src="https://assets.thestandnews.com/media/photos/5cadae85401a6c52542d6c7.jpg"><figcaption><span>紀錄片《別當我公主》（We Are Not Princesses）圖片</span></figcaption></figure></div>

<p>《別》和《秘洞醫院》其中一個共通點，是刻劃了脆弱的處境和堅毅的心性同時體現於敘利亞女性身上。《秘》的主角 Amani 時刻活在這種張力之中。她是醫生，因為空襲而把醫院遷往地底。雖然她能幹又熱心，救急扶危之餘，還要面對各方「女性應安份持家看孩子」的成見。結果 Amani 及其他女同事，在醫護工作以外，也要為團隊煮食和照顧孩子。承受最多最重的擔子的人，才是最堅強的人。恃強凌弱者，其實是懦夫。</p>

<p>然而太平盛世不需要英雄，公平的社會也不應由某一類人承受更多的重擔。《別》裡參加戲劇工作坊的女性難民，也反思著自己與安蒂岡妮之間的距離。她們是普通主婦，安蒂岡妮則是貴族出身，其超然的剛烈與情操非常人能及。安蒂岡妮在安葬其兄長時，身份微妙，因為她已不是公主，卻是現任國王之子的未婚妻，本可保持皇家地位。不過安蒂岡妮為了「天道」而忤逆國王之意，決絶地放棄一切。她的故事不是道德教化寓言，而是悲劇，而悲劇穿透了古今虛實，無人能避。若視安蒂岡妮為當效法之榜樣，固然叫人難以企及，但她是一個富有啟發性的參照，若暗夜之星辰，像廣場的母親。</p>

<p>註：《別當我公主》和《秘洞醫院》是聯合國難民署「第十四屆慈善難民電影節」的網上放映節目。</p>

<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/TvMok0QDvLA" title="YouTube video player" width="560"></iframe></p>

<blockquote>
<p><strong>延伸參考：</strong></p>

<p><strong>1. 蘇弗克里茲（Sophocles） 著，呂健忠 譯：《安蒂岡妮 — 墓窖裡的女人》。台北：書林出版，1988 年。</strong></p>

<p>Sophocles 是古希臘三大悲劇詩人之一，與 Euripides 及 Aeschylus 齊名。這譯本除了《安蒂岡妮》的劇本，也附上背景導讀及作者小傳，讓讀者更容易掌握此部名作的精粹。</p>

<p><strong>2. Žižek, Slavoj. <em>Antigone</em>. London: Bloomsbury Academic, 2016.</strong></p>

<p>哲學家齊澤克重寫《安蒂岡妮》，受戲劇家布萊希特啟發，給她安排三種不同的可能結局，探討政權與人民的關係。</p>

<p><strong>3. Waad Al-Kateab、Edward Watts 導：《致莎瑪︰敘利亞家書》（For Sama）。英國、美國、敘利亞，2019 年。</strong></p>

<p>導演拍攝自己一家在敘利亞內戰時被圍困阿勒坡城的過程。她的醫生丈夫在廢墟中設立戰時醫院，而她在期間一邊拍攝，一邊懷孕產女。她給女兒改名 Sama，意思是天空，象徵著希望。</p>
</blockquote>

<p>&nbsp;</p>

<p>原載於<a href="https://christiantimes.org.hk/Common/Reader/News/ShowNews.jsp?Nid=166022&amp;Pid=2&amp;Version=1766&amp;Cid=1156&amp;Charset=big5_hkscs" target="_blank">《時代論壇》1766 期</a></p>]]></content>
        <author>
            <name>賴勇衡</name>
            <uri>https://www.thestandnews.com/author/%E8%B3%B4%E5%8B%87%E8%A1%A1</uri>
        </author>
        <category label="國際"/>
        <published>2021-07-10T07:15:48.543Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[《醉美的一課》— 失控作為一種人生哲學]]></title>
        <id>https://www.thestandnews.com/culture/%E9%86%89%E7%BE%8E%E7%9A%84%E4%B8%80%E8%AA%B2-%E5%A4%B1%E6%8E%A7%E4%BD%9C%E7%82%BA%E4%B8%80%E7%A8%AE%E4%BA%BA%E7%94%9F%E5%93%B2%E5%AD%B8</id>
        <link href="https://www.thestandnews.com/culture/%E9%86%89%E7%BE%8E%E7%9A%84%E4%B8%80%E8%AA%B2-%E5%A4%B1%E6%8E%A7%E4%BD%9C%E7%82%BA%E4%B8%80%E7%A8%AE%E4%BA%BA%E7%94%9F%E5%93%B2%E5%AD%B8"/>
        <updated>2021-09-17T09:29:44.007Z</updated>
        <summary type="html"><![CDATA[酒精無國界，丹麥電影《醉美的一課》（Another Round）以飲酒為題，醉落存在意義的大哉問。在戲裡，酒是一道橋，通往的另一端才是電影的核心：失控。酒精使人自控力降低，可以令平常拘謹的人放鬆心情，輕談淺酌不夜天。在中學教書的男主角 Martin 和另外三位同事兼好友，人到中年，漸漸失去生命的熱情。他們試圖按學者 Finn Skårderud 提出的「人天生酒精濃度過低，最好提升到 0.05%…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/112907_OwNj6.png"><figcaption><span>電影《醉美的一課》（Another Round）劇照</span></figcaption></figure><p>酒精無國界，丹麥電影《醉美的一課》（Another Round）以飲酒為題，醉落存在意義的大哉問。在戲裡，酒是一道橋，通往的另一端才是電影的核心：失控。</p>

<p>酒精使人自控力降低，可以令平常拘謹的人放鬆心情，輕談淺酌不夜天。在中學教書的男主角 Martin 和另外三位同事兼好友，人到中年，漸漸失去生命的熱情。他們試圖按學者 Finn Skårderud 提出的「人天生酒精濃度過低，最好提升到 0.05%」假說做實驗，突破人生困局。他們最初小心控制份量，用酒精濃度計測量調控，結果真的在工作時充滿能量，更改善了學生的學習氣氛。這階段，導演以字幕卡顯示角色在電腦輸入實驗資料和酒精測量的濃度變化，呈現出科學的、理性的感覺。</p>

<p>Martin 等人這種方法，是以酒為藥，但酒是失控之藥，他們要做的是控制「失控」本身，注定徒勞。 控制是功利的，講求效益，要理性計算，往往有解決問題的實際考慮。Martin 的問題是甚麼？丹麥是世上快樂指數最高的幾個國家之一，民主自由、福利高、工時短、環境優美，普通中學教師能住過千呎大屋。但 Martin 仍不快樂。戲裡提及過丹麥哲學家祈克果（Søren Kierkegaard，又譯「齊克果」），曾深思「存在的焦慮」（Angst，又譯「憂懼」）這課題。這是一種沒有具體對象或原因的焦慮，屬於自由的人，而自由的另一面是茫然。</p>

<p>但 Martin 的狀態更像是一種苦悶（ennui）。他年輕時是優秀學生，卻因為家庭責任而放棄了博士奬學金，昨日之日不可留。多年以後，家人關係淡薄，學生不尊重他，今日之日多煩憂。他的三個老友境況類似，欲以酒作引，精確控制，重燃熱情。但這是矛盾的，因為酒令人失控，越搞越禍，最終導致其中一人投海自殺的悲劇。他們曾提及文豪海明威，欲效法他三杯下肚思如泉湧，卻忘記他最後吞槍自盡的悲慘下場。</p>

<p>戲裡戲外，人們面對的還有另一困境：荒謬。導演 Thomas Vinterberg 本來想拍美酒謳歌，在女兒學校取景，讓女兒及一眾同學參演。怎料開鏡後幾天，其女兒遇上交通意外身亡。命運非人所能掌控，逝去的親人和時光皆無法追回，喝更多酒也不能澆愁。人生無意義，理想求不得，所愛痛別離；盡力無解決，弄巧反更拙，這就是存在的荒謬。</p>

<p>電影最後以 Martin 展開雙臂飛躍向海的定鏡作結，令人想起祈克果對荒謬的終極回應：「信仰之躍」（Leap of faith），在未知迷茫之崖跳向上帝的懷抱。既無去路，不如放手一博；Martin 婚姻瀕臨破裂，仍盡力示愛挽回，柳暗花明；他另一個同事，恐怕一個學生因考試壓力會再次崩潰，讓他偷偷喝酒壯膽，又過一關。</p>

<p>但《醉美的一課》不是祈克果派福音電影，角色們跳海或跳舞，都沒有跳到宗教信仰那邊，反而蹈向說「上帝已死」的尼采（Friedrich Nietzsche）。最後一班學生畢業，在城內巡遊慶祝，飲多杯勝嘅喜氣洋洋。剛為故友扶靈的 Martin 等人，哀慟有時，跳舞有時，決定與喜樂的學生們同樂。尼采以希臘神話中的酒神 Dionysus 為熱情擁抱生命的象徵，與代表理性的太陽神 Apollo 相對。不是真的要復甦古希臘的酒神祭祀，而是從中提煉出一種人生態度：既然世界無可避免充滿苦難，遠非人所能掌控，不如盡情擁抱命運 — 包括其中的痛苦與失落。這不是試圖控制「失控」，而是肯定「失控」本身。</p>

<p>飾演男主角的 Mads Mikkelsen 對角色情感變化的演繹細膩；他是舞者出身，在最後一場縱情起舞，盡顯影帝風采。結局飛躍出海，與他那投海自盡的朋友互相呼應。後者男人老狗、孤家寡人，絶望而殁；Martin 則是重燃希望，釋懷而躍。導演沒有拍攝兩者墮進水裡的情景；對自殺者而言，暗場交代，避開了死亡的最傷痛一刻。而男主角的最後飛躍以定鏡作凝在半空，彷彿永不會下墮；攝影機角度上揚，不見海面，只見天空，如鷹展翅上騰。</p>

<blockquote>
<p><strong>延伸閱讀：</strong></p>

<p><strong>1. 齊克果（Søren Kierkegaard）：《恐懼和戰慄》。台北：商務，2017年。</strong></p>

<p>齊克果在這本書中提出「信仰之躍」，以亞伯拉罕獻以撒的例子，從普遍的倫理「跳躍」到對上帝的絶對順服，哪怕被世人視為瘋狂。他把亞伯拉罕稱為「信仰騎士」，與悲劇英雄相對。</p>

<p><strong>2. Søren Kierkegaard. <em>The Concept of Anxiety</em>. New York: Liveright, 2015.</strong></p>

<p>齊克果在這本書中分析基督教的罪觀，發展出他的焦慮（Angst）的概念，探討焦慮與自由意志的關係。</p>

<p><strong>3. 尼采（Friedrich Nietzsche）：《悲劇的誕生》。台北：安婕工作室，2017年。</strong></p>

<p>尼采的第一本書，以希臘神話中的太陽神和酒神之相生相尅開展他對存在的思考，並以希臘悲劇反思現代社會的文化危機。</p>
</blockquote>

<p>&nbsp;</p>

<p>原載於<a href="https://christiantimes.org.hk/Common/Reader/News/ShowNews.jsp?Nid=165746&amp;Pid=102&amp;Version=0&amp;Cid=2011&amp;Charset=big5_hkscs" target="_blank">《時代論壇》1762 期</a>，原題為〈唯有飲者留其名〉</p>]]></content>
        <author>
            <name>賴勇衡</name>
            <uri>https://www.thestandnews.com/author/%E8%B3%B4%E5%8B%87%E8%A1%A1</uri>
        </author>
        <category label="文化"/>
        <published>2021-06-12T09:10:51.000Z</published>
    </entry>
</feed>