<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>https://www.thestandnews.com/author/angel/rss</id>
    <title>立場新聞 Stand News - Angel</title>
    <updated>2021-12-06T13:49:17.500Z</updated>
    <generator>https://github.com/jpmonette/feed</generator>
    <link rel="alternate" href="https://www.thestandnews.com/author/angel"/>
    <subtitle>《立場新聞》編採獨立自主，不受任何贊助人、財團、權力機構及黨派左右。我們以非牟利原則營運，所有經營盈餘和贊助，只會用於傳媒事業。</subtitle>
    <rights>© 2021 立場新聞. All rights reserved.</rights>
    <entry>
        <title type="html"><![CDATA[港韓電影背後的功臣們 — 「新浪潮．新海岸」大師班與座談會]]></title>
        <id>https://www.thestandnews.com/culture/%E6%B8%AF%E9%9F%93%E9%9B%BB%E5%BD%B1%E8%83%8C%E5%BE%8C%E7%9A%84%E5%8A%9F%E8%87%A3%E5%80%91-%E6%96%B0%E6%B5%AA%E6%BD%AE%E6%96%B0%E6%B5%B7%E5%B2%B8%E5%A4%A7%E5%B8%AB%E7%8F%AD%E8%88%87%E5%BA%A7%E8%AB%87%E6%9C%83</id>
        <link href="https://www.thestandnews.com/culture/%E6%B8%AF%E9%9F%93%E9%9B%BB%E5%BD%B1%E8%83%8C%E5%BE%8C%E7%9A%84%E5%8A%9F%E8%87%A3%E5%80%91-%E6%96%B0%E6%B5%AA%E6%BD%AE%E6%96%B0%E6%B5%B7%E5%B2%B8%E5%A4%A7%E5%B8%AB%E7%8F%AD%E8%88%87%E5%BA%A7%E8%AB%87%E6%9C%83"/>
        <updated>2021-11-26T02:49:22.655Z</updated>
        <summary type="html"><![CDATA[當電影在頒獎禮裡獲取獎項，站在台上致辭的雖然可能只有一人，但背後卻是無數電影人集體的心血結晶。一部優秀電影能夠從想法成為現實，在激烈競爭中脫穎而出，並來到觀眾眼前，著實是無數人的功勞。無論是大家比較熟悉的演員、導演、監製、編劇、攝影指導，還是默默付出的宣傳人員、發行商和銷售，甚至是推動業界交流、提供資助與融資平台、向公眾推廣電影的影展與電影市場，全部缺一不可。釜山國際電影節，是亞洲最重要的電影節…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/259303593_1133948594041454_2341677241328997741_n.jpeg"><figcaption><span>「新浪潮．新海岸︰釜山國際電影節」開幕活動（圖片來源：香港藝術中心：流動影像 Facebook）</span></figcaption></figure><p>當電影在頒獎禮裡獲取獎項，站在台上致辭的雖然可能只有一人，但背後卻是無數電影人集體的心血結晶。一部優秀電影能夠從想法成為現實，在激烈競爭中脫穎而出，並來到觀眾眼前，著實是無數人的功勞。無論是大家比較熟悉的演員、導演、監製、編劇、攝影指導，還是默默付出的宣傳人員、發行商和銷售，甚至是推動業界交流、提供資助與融資平台、向公眾推廣電影的影展與電影市場，全部缺一不可。</p>

<p>釜山國際電影節，是亞洲最重要的電影節之一，由1996年起舉辦至今已25年，見證並推動著韓國以至亞洲電影工業發展。香港藝術中心的「新浪潮．新海岸」節目，今屆與釜山國際電影節合作，由資深影評人及電影策展人Maggie Lee策劃整個節目，並與釜山國際電影節節目總監南東喆聯合策劃韓國電影放映。節目且包括一系列大師班與座談會，邀請香港、南韓及海外電影業界工作者，分享他們在不同崗位上由劇本創作以至節目策劃、由推廣銷售以至培育新進影人等經驗。</p>

<p>「劇本是電影的靈魂」，沒有編劇賦予靈魂，再出色的表演與場面調度也不能構築起一部電影迷人的世界觀與扣人心弦的情節。寫劇本不易，文字改編電影就更具挑戰性。畢竟小說的篇幅與敘事手法與電影大相逕庭，如何取捨、轉化原著，以至發掘故事中值得探討的主題，決非易事。韓國編劇丁瑞慶曾以法國作家左拉的小說《Thérèse Raquin》為靈感，改編成朴贊郁導演的《饑渴誘罪》（2009）；香港導演陳果亦與作家李碧華合作，把其短篇小說《餃子》改編成同名電影。在「改編劇本大師班︰丁瑞慶與陳果」中，兩位影人將以兩部放映電影為例子，分享他們在編劇及改編劇本上的創作心得。</p>

<p>幕後的電影工作者，即使只是談電影製作以外的人員，團隊之龐大、分工之細也令人驚嘆，而每個崗位對電影的成功與否都極其重要。以電影推廣與培育為例，今天席捲全球的韓流旋風、甚至歷史性出產首部非英語奧斯卡最佳電影絕非偶然，背後全靠多年各方人士的深耕細作，其中除了過去30年於國際舞台上推廣及提升韓國電影地位的釜山國際電影節，還有1970年成立的官方組織韓國電影振興委員會（KOFIC），以提供補助的方式為本土電影提供資金，提升電影質量。「韓國電影於世界舞台上的展現及推廣」座談會，請來釜山國際電影節節目總監南東喆與韓國電影振興委員會支援事業本部總監張光壽對談，討論兩個機構於近年如何繼續推動韓國電影工業發展。另一個座談會「釜山國際電影節培育亞洲電影人之使命」則請來亞洲電影資助計畫（Asian Project Market）經理朴世利，細談釜山國際電影節為亞洲電影市場而設的資助基金與各項活動。</p>

<p>在韓國以外，香港以至國際電影業界又如何推廣當地製作，讓好電影以至整個電影工業能得以延續，甚至放眼世界？一方面，我們深信電影的藝術性，另一方面，亦不能對其商業性視而不見——畢竟要讓一個小小意念成真，以至喚起大眾共鳴，所需人力物力不菲。對創作人來說，電影是夢想，但對資金方來說，電影亦是投資。再藝術性的作品，也難以避免要考慮商業運作，這對新進電影人來說尤其困難。在「新銳導演的宣傳推廣」座談會裡，策展人Maggie Lee將與本地及國際發行商及監製的Isabelle Glachant（Asian Shadows及Chinese Shadows創辦人）、黃茂昌（前景娛樂創辦人、電影監製）與雷澤緯（SCREENWORKS影響原創影視股份有限公司銷售顧問），暢談他們橫跨歐亞的電影銷售與發行經驗。</p>

<p>最後，在商業運作以外，影展策劃亦在電影推廣與交流中擔任非常重要的角色。如何推廣不同的電影文化，同時讓各地作品能在影展中展開交流，這涉及的考慮絕不簡單。「新浪潮．新海岸」兩位策展人Maggie Lee與南東喆將會在「港韓電影的融匯交流」中分享他們在這次節目以及過往的策展工作中的心得，並由國際交流經驗豐富的本地監製與導演楊曜愷主持，討論港韓兩地電影在歷史、文化、美學上的交流與影響。&nbsp;</p>

<p>&nbsp;</p>

<p><strong>「新浪潮．新海岸︰釜山國際電影節」</strong></p>

<p>日期︰2021.11.25 - 2022.01.16<br>
地點︰香港藝術中心古天樂電影院&nbsp;&nbsp;</p>

<div><figure><img src="https://assets.thestandnews.com/media/photos/259425591_1133975690705411_4698017069040022138_n.jpeg"><figcaption><span>「新浪潮．新海岸︰釜山國際電影節」海報（圖片來源：香港藝術中心：流動影像 Facebook）</span></figcaption></figure></div>

<p>於香港藝術中心網站瀏覽節目詳情：<a href="https://hkac.org.hk/calendar_detail/?u=3-2UYoCLubI">https://hkac.org.hk/calendar_detail/?u=3-2UYoCLubI</a><br>
於撲飛POPTICKET購票：<a href="https://www.popticket.hk/biff">https://www.popticket.hk/biff</a><br>
香港藝術中心：流動影像 Facebook：<a href="https://www.facebook.com/hkartscentremovingimages">https://www.facebook.com/hkartscentremovingimages</a></p>]]></content>
        <author>
            <name>Angel</name>
            <uri>https://www.thestandnews.com/author/angel</uri>
        </author>
        <category label="文化"/>
        <published>2021-11-26T02:48:55.765Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[南韓的繁華與陰暗]]></title>
        <id>https://www.thestandnews.com/culture/%E5%8D%97%E9%9F%93%E7%9A%84%E7%B9%81%E8%8F%AF%E8%88%87%E9%99%B0%E6%9A%97</id>
        <link href="https://www.thestandnews.com/culture/%E5%8D%97%E9%9F%93%E7%9A%84%E7%B9%81%E8%8F%AF%E8%88%87%E9%99%B0%E6%9A%97"/>
        <updated>2021-05-27T09:54:08.000Z</updated>
        <summary type="html"><![CDATA[每一個高度發展的國度，背後都有被刻意忽視的角落。摩天大樓越高聳入雲，GDP升幅越叫人亢奮，底下的人就越發模糊，卑微得像塵埃，隨手一掃就消失於視線之外。南韓經濟自1997年再起飛至今，高科技產業發展一日千里，文化產業打入全球市場，疫情前本地生產總值年年節節上升。 同時，青年失業率高企，社會壓力叫人精神繃緊，轉型正義未完成，曾受迫害的人始終活在社會邊緣。香港藝術中心「韓女獨有戲：韓國女性獨立電影系列…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/kor-16_pPNTh.png"></figure><p>每一個高度發展的國度，背後都有被刻意忽視的角落。摩天大樓越高聳入雲，GDP升幅越叫人亢奮，底下的人就越發模糊，卑微得像塵埃，隨手一掃就消失於視線之外。南韓經濟自1997年再起飛至今，高科技產業發展一日千里，文化產業打入全球市場，疫情前本地生產總值年年節節上升。 同時，青年失業率高企，社會壓力叫人精神繃緊，轉型正義未完成，曾受迫害的人始終活在社會邊緣。</p>
<p>香港藝術中心「韓女獨有戲：韓國女性獨立電影系列」六、七月節目，把目光放到南韓經濟成功底下的殘酷現實。透過三部長片與一部短片集，以女性影人角度探討南韓不同層面的社會狀況。</p>
<p>經濟低迷引發人間悲劇，但經濟上揚卻不代表歌舞昇平，只不過可憐的人彷彿微不足道罷了。奉俊昊《上流寄生族》說：人有錢才有餘裕善良。活在食物鏈末端，是否註定只能掙扎向上？全高雲《小公女》的女主角微笑生活只要有酒、香煙還有男朋友就滿足，樂天知命卻不見得她窮風流。她曾有過音樂的夢想，有過一起相知交心的友伴，但20多歲時捱麪包叫歷練，30多歲仍然三餐不繼被視作不負責任。香煙大幅加價，微笑微薄的收入負擔不起香煙與租金兼得，唯有推著行李箱借住一個接一個舊日夥伴的家，有人自顧不暇，有人提議給她安穩，有人罵她爛泥扶不上牆。我們羨慕長不大的小飛俠，可能只因為夢幻島沒有通賬。社會巨輪高速前進，我們跑得不夠快便落入貧窮線。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/Microhabitat2028329_O8Vqb.jpg"><figcaption><span>全高雲《小公女》電影劇照</span></figcaption></figure><p></p>
<p>南韓經濟自60年代起朴正熙獨裁執政18年間快速上揚，被喻為「漢江奇蹟」。期間，南韓於1965年參與越南戰爭，成為南越繼美國後最大盟軍，獲得軍援與韓商於當地賺取外匯的機會，令韓戰後經濟疲弱的南韓搭上高速發展之列車。參與越戰，被視為漢江奇蹟的契機。奇蹟的另一面，南韓軍隊於越戰期間多次屠殺當地居民，死亡人數難以統計。李吉寶羅《回憶之戰》以這段被刻意淡忘的歷史為題，訪問越戰倖存者，了解他們的傷痛與真相，見證他們這段回憶之戰。其中兩個越戰時仍是小女孩的倖存者，遠赴南韓於法官、公眾以至前韓兵面前申訴，為自己和家人爭取公義。近60年後，她們對慘劇發生當刻種種仍歷歷在目。時間不能沖淡歷史，改善經濟的「功」也不能抵濫殺無辜的「過」，唯有承認真相才能重新出發。每一個走過獨裁統治的國家，都需要面對轉型正義這個艱難卻必須回答的題目。</p>
<p>以為時間可以沖淡一切，但忘記不等於痛苦消失。金東鈴與朴勁泰虛實交錯的紀錄片《鬼怪與懷孕的樹》，隨著朴仁順的曲折人生展開一場記憶、遺忘與療傷之旅。朴仁順在韓戰期間被父母遺棄河邊，被賣到美國軍營小鎮當妓女，後來懷孕結婚遠嫁他鄉，輾轉又回到南韓。回國初期還有打工，現在則拾紙皮維生。記憶對她來說是猶如迷霧：問到美國生活的種種，她只依稀記得丈夫家暴與奶奶的名字；也早已忘了當年是誰把她賣掉，是壞人還是自己？甚至「朴仁順」亦可能不是她真名——這是印在馬伕給她買來的出世紙上的名字。這個失去過去的人，仍然在畫作中透露出內心創傷。無論對人或社會來說，有時遺忘並非祝福，而是為了面對不能承受之事的自我保護機制。 但傷口不會因為被忘記而痊癒。今天富裕的南韓境內，曾受戰爭傷害的人仍被遺棄在社會邊緣。歌舞昇平的燈光永遠照不到他們頭上，而他們的故事正在漸漸消失。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/Goblins_Pregnant20Tree_Xwi7f.jpg"><figcaption><span>金東鈴與朴勁泰紀錄片《鬼怪與懷孕的樹》</span></figcaption></figure><p></p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/Unpredictable20Boy_still28529_WOzVn.jpg"><figcaption><span>弓有庭短片《不規則男孩》</span></figcaption></figure><p></p>
<p>就算有幸擁有一段平凡人生，講求效率與集體性的高速社會，仍能把人壓得透不過氣。弓有庭的短片《不規則男孩》以一個看似滿肚謊言的小男孩與老師的攻防戰，戳破當今人們互相信任的假象——我當然相信你，只要你不騙我。但要肯定一個人沒有說謊，談何容易？畢竟說點小謊並沒有大礙，只要不是自己受騙就好。金顯定《入門班》透過少女佳英與寫作班同學之互動，點出社會偽善冷酷的一面。少女努力默默創作LGBTQ+故事，又遇上同學情挑，以為終於有人明白自己。但安全封閉的創作世界與殘酷的戀愛現實並不一樣，她的初戀不過是對方遊戲一場。只因為她喜歡女生，在仍相對封閉保守的南韓社會，苦水都只能自己吞。我們一邊說「做自己就好」、「有自信就美」，一邊偏愛美麗、外向的人。性格、相貌和家世與生俱來、人人不同，但成長時我們偏偏期望所有人能符合某些特定階模，才稱得上合群。雖然世界許多時不太美好，幸好鄭多喜《天馬行空人樹狗》以生活最簡單片段，為我們帶來腦袋喘息的空間，在放空中暫時放下自身與社會的煩惱。</p>
<p>生於同樣繁榮的香港，對以上種種處境總找到共鳴之處。也許身在福中不知福是真的，但貧富懸殊確切存在；社會勢利無情也是真的，但總有人在富裕社會的邊緣掙扎。總要記得，歲月靜好，仍總有人與事不應遺忘。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/Beginners2720Class_still28429_6txJ4.jpg"><figcaption><span>金顯定《入門班》</span></figcaption></figure><p></p>
<p>&nbsp;</p>
<p>於香港藝術中心網站瀏覽節目詳情：<a href="http://bit.ly/3b3UeJn" target="_blank">http://bit.ly/3b3UeJn</a>&nbsp; / <a href="http://www.hkac.org.hk" target="_blank">www.hkac.org.hk</a></p>
<p>於 Hong Kong Movie香港電影購票：<a href="http://bit.ly/3rDMLGI" target="_blank">http://bit.ly/3rDMLGI</a>&nbsp; / <a href="http://www.hkmovie6.com" target="_blank">www.hkmovie6.com</a></p>
<p>香港藝術中心：流動影像 Facebook：<a href="https://www.facebook.com/hkartscentremovingimages" target="_blank">https://www.facebook.com/hkartscentremovingimages&nbsp;</a></p>
<p>（本文為贊助內容。文章為作者觀點，不代表主辦機構立場。）</p>
<p>#韓國&nbsp; #藝術中心</p>
]]></content>
        <author>
            <name>Angel</name>
            <uri>https://www.thestandnews.com/author/angel</uri>
        </author>
        <category label="文化"/>
        <published>2021-05-27T05:23:34.000Z</published>
    </entry>
</feed>