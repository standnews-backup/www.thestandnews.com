<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>https://www.thestandnews.com/author/edwin-chow/rss</id>
    <title>立場新聞 Stand News - 鄒之喬</title>
    <updated>2021-12-06T22:58:37.127Z</updated>
    <generator>https://github.com/jpmonette/feed</generator>
    <link rel="alternate" href="https://www.thestandnews.com/author/edwin-chow"/>
    <subtitle>《立場新聞》編採獨立自主，不受任何贊助人、財團、權力機構及黨派左右。我們以非牟利原則營運，所有經營盈餘和贊助，只會用於傳媒事業。</subtitle>
    <rights>© 2021 立場新聞. All rights reserved.</rights>
    <entry>
        <title type="html"><![CDATA[阿富汗的「Digital Dunkirk」民間行動]]></title>
        <id>https://www.thestandnews.com/international/digital-dunkirk-%E7%9A%84%E6%B0%91%E9%96%93%E8%A1%8C%E5%8B%95</id>
        <link href="https://www.thestandnews.com/international/digital-dunkirk-%E7%9A%84%E6%B0%91%E9%96%93%E8%A1%8C%E5%8B%95"/>
        <updated>2021-08-31T10:57:09.558Z</updated>
        <summary type="html"><![CDATA[隨著各國在阿富汗撤退的限期逼近，有關阿富汗的政治局勢，人道災難及其難民的報道鋪天蓋地。筆者想分享一些民間努力，藉此機會補添一般報導鮮為人知的一面，擴闊香港人的國際視野。阿富汗的局勢急轉直下，阿富汗人對塔利班的極權統治極其恐懼，爭相用各種方法逃離家園，其中以首都喀布爾機場為主要渠道。以美國為首的撤離行動，除了正式的官方行動，早前《華盛頓郵報》也報導了美國民間的「Digital Dunkirk」行動…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/AP21235320086463.jpg"><figcaption><span>2021 年 8 月 23 日，有阿富汗人透過查曼（Chaman）口岸進入了巴基斯坦境內，並等候交通接送。
(AP Photo/Jafar Khan)</span></figcaption></figure><p>隨著各國在阿富汗撤退的限期逼近，有關阿富汗的政治局勢，人道災難及其難民的報道鋪天蓋地。筆者想分享一些民間努力，藉此機會補添一般報導鮮為人知的一面，擴闊香港人的國際視野。</p>

<p>阿富汗的局勢急轉直下，阿富汗人對塔利班的極權統治極其恐懼，爭相用各種方法逃離家園，其中以首都喀布爾機場為主要渠道。以美國為首的撤離行動，除了正式的官方行動，早前<a href="https://www.washingtonpost.com/technology/2021/08/26/veteran-volunteers-digital-dunkirk-afghanistan/" target="_blank">《華盛頓郵報》</a>也報導了美國民間的「Digital Dunkirk」行動，由老兵和民間義工組成，主要目的為希望離開阿富汗的人士提供實時有用的資訊。</p>

<div><figure><img src="https://assets.thestandnews.com/media/photos/01ggshdkhgkd.jpg"></figure></div>

<div><figure><img src="https://assets.thestandnews.com/media/photos/02ggshdkhgkd.jpg"></figure></div>

<p>雖然此民間行動倉促成軍，但組織和規模卻算是分工有序，也利用了各種科技來收集資料，實時通訊，處理數據和釐定決策。透過當地人的聯絡，有意離開的人可以用電郵或網上表格申報個人資料，有專人為以千計的每一個救援請求分類及回覆，其資料會存入預設表格有待整理。要求撤離援助的人包括高官達人和草根平民，女性固然恐懼新政權的統治，普通人也懼怕與前朝政府或外國機構的關係會迎來塔利班報復，很多陳情的資料甚至附上一些實質威脅的證據。資料顯示大部人扶老攜幼，也有孕婦嬰孩，在某些情況下更附上實時的位置數據等待救援。</p>

<div><figure><img src="https://assets.thestandnews.com/media/photos/03ggshdkhgkd.jpg"></figure></div>

<p>收集到的數據會加以整理覆核。民間團隊會利用最新的衛星數據，眾包資料（crowdsourcing）和環境數據，繪製實時地圖（如塔利班的檢測站，機場的閘口情況等等），分析行動路徑，提供實時資訊。這民間行動也有很多現役的軍官參與，受助人透過民間和官方的協作，增加成功撤離的機會。不幸地，有部份人在旅途中已遇上不測，他們旅途的艱辛和凶險，即使透過電腦熒幕也能感受一點。</p>

<div><figure><img src="https://assets.thestandnews.com/media/photos/04ggshdkhgkd_Fjz0qlp.jpg"></figure></div>

<p>以上只是實際情況的一部分。筆者想分享幾點：</p>

<ol>
	<li>當然常人難以想像阿富汗人所面臨的人道災難，但他們實在需要國際社會長期關注和援助。</li>
	<li>即使身處千里之外，筆者處理數據時也難免牽動個人情緒，寫文章對筆者而言其實是一個抽離思緒和自我療癒的過程，適當地解壓實屬必要。面對威權管治，很多香港人高壓的情緒仍有待處理。</li>
	<li>礙於時間所限，這次人道危機所收集的數據其實沒有經過很高深的電腦分析，很多義工透過肉眼和常識已能整理有用的資料讓受助人作決定。地理數據的好處是容易透過地圖來顯示（visualize）和理解空間格局與關係（spatial pattern and relationship）。幸好現在已有很多免費和開源平台可以收集及顯示空間數據，加上手機和眾包文化的普及，民間的高手可以輕易各展所長。在地理學裏，「everything happened somewhere」，即所有的事情都發生在某個地方，可以預視將來的應用會需要更多地理數據。</li>
</ol>]]></content>
        <author>
            <name>鄒之喬</name>
            <uri>https://www.thestandnews.com/author/edwin-chow</uri>
        </author>
        <category label="國際"/>
        <published>2021-08-31T10:49:58.933Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[一二三五七地圖]]></title>
        <id>https://www.thestandnews.com/china/%E4%B8%80%E4%BA%8C%E4%B8%89%E4%BA%94%E4%B8%83%E5%9C%B0%E5%9C%96</id>
        <link href="https://www.thestandnews.com/china/%E4%B8%80%E4%BA%8C%E4%B8%89%E4%BA%94%E4%B8%83%E5%9C%B0%E5%9C%96"/>
        <updated>2021-06-14T08:35:48.000Z</updated>
        <summary type="html"><![CDATA[筆者近年因為數人頭這偏門的興趣，自 2016 年起開始數算出席每年六四燭光晚會的人數，直至今年維園內容不下一點燭光，唯有暫別這夕陽行業的工作。然而，維園外燭光處處，證明香港人擇善固執。筆者在此推介三張地圖，以延續遺忘與記憶的鬥爭。圖一顯示北京西長安街在六月三日晚上至六月四日零晨軍隊與學生及市民的動向，屠殺主要就在這條街道上。圖二記載六月四日凌晨軍隊進入天安門廣場後開始清場，駐守廣場內的學生撤退的…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/0_it0Pw.png"><figcaption><span>圖片素材來源：支聯會網站、六四檔案網站、The New York Sun</span></figcaption></figure><p>筆者近年因為數人頭這偏門的興趣，自 2016 年起開始數算出席每年六四燭光晚會的人數，直至今年維園內容不下一點燭光，唯有暫別這夕陽行業的工作。然而，維園外燭光處處，證明香港人擇善固執。筆者在此推介三張地圖，以延續遺忘與記憶的鬥爭。</p>
<p style="text-align:center"></p><figure><img src="https://assets.thestandnews.com/media/photos/1_5UK3m.jpg"><figcaption><span>圖一。西長安街（來源：六四檔案 https://www.64memo.com/b5/8266.htm ）</span></figcaption></figure><p></p>
<p>圖一顯示北京西長安街在六月三日晚上至六月四日零晨軍隊與學生及市民的動向，屠殺主要就在這條街道上。</p>
<p style="text-align:center"></p><figure><img src="https://assets.thestandnews.com/media/photos/2_PfNnm.jpg"><figcaption><span>圖二。廣場（來源：六四檔案 https://www.64memo.com/b5/8266.htm ）</span></figcaption></figure><p></p>
<p>圖二記載六月四日凌晨軍隊進入天安門廣場後開始清場，駐守廣場內的學生撤退的路向。</p>
<p style="text-align:center"></p><figure><img src="https://assets.thestandnews.com/media/photos/3_nCCDI.jpg"><figcaption><span>圖三。六四死難者的分佈圖（來源：The New York Sun https://www.nysun.com/files/massacremap.pdf ）</span></figcaption></figure><p></p>
<p>圖三為部分六四死難者的分佈。此死難者名單的地圖乃根據倖存者及天安門母親的資料製成，雖然地圖只列出 47 位能追溯其遇害地點及有限的個人資料（如名字或年齡），但根據<a href="http://tiananmenmother.org/" target="_blank">天安門母親</a>的網站，截止至 2011 年 8 月，六四死難者名册共有 202 位。未能核實名字的死難者，須由兩位或以上的見證人方能驗證，由於核實過程嚴謹，真實死難者的人數當不只此數。</p>
<p>這些資料在紀念館被封和有人聲稱 8964 當年沒有死難者的今天尤為重要。如對估計六四死難人數有興趣的讀者，可參考這篇<a href="https://www.storm.mg/article/1338245?mode=whole" target="_blank">文章</a>。</p>
<p>謹以此文悼念。</p>
]]></content>
        <author>
            <name>鄒之喬</name>
            <uri>https://www.thestandnews.com/author/edwin-chow</uri>
        </author>
        <category label="中國"/>
        <published>2021-06-14T08:35:48.000Z</published>
    </entry>
</feed>