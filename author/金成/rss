<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>https://www.thestandnews.com/author/金成/rss</id>
    <title>立場新聞 Stand News - 金成</title>
    <updated>2021-12-09T09:22:47.407Z</updated>
    <generator>https://github.com/jpmonette/feed</generator>
    <link rel="alternate" href="https://www.thestandnews.com/author/金成"/>
    <subtitle>《立場新聞》編採獨立自主，不受任何贊助人、財團、權力機構及黨派左右。我們以非牟利原則營運，所有經營盈餘和贊助，只會用於傳媒事業。</subtitle>
    <rights>© 2021 立場新聞. All rights reserved.</rights>
    <entry>
        <title type="html"><![CDATA[她的口是苦的]]></title>
        <id>https://www.thestandnews.com/culture/%E5%A5%B9%E7%9A%84%E5%8F%A3%E6%98%AF%E8%8B%A6%E7%9A%84</id>
        <link href="https://www.thestandnews.com/culture/%E5%A5%B9%E7%9A%84%E5%8F%A3%E6%98%AF%E8%8B%A6%E7%9A%84"/>
        <updated>2021-12-08T09:53:17.376Z</updated>
        <summary type="html"><![CDATA[之前看林嘉欣面書，甚麼兩個女孩的母親，患上乳癌，嚇了一跳。然後才知道她在述說《美國女孩》演的角色。再想想，其實沒甚麼大驚小怪，活在這個年代活到這些年歲，有關個人危疾、全球疫情、移民和回流以至家人散聚等這些事，總有一項我們必然頻繁面對。《美國女孩》好多鏡頭都讓人悽然，只是基礎的一家四口之相愛和傾軋，足以牽扯每個人內心的巨浪。尤其我們跟家人一起睡覺一起吃飯，迫於無奈齊上齊落，會積累深厚而不可取代的牽…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/3P7VHCTQo16jgWH432423bG4RH-6e.png"><figcaption><span>電影《美國女孩》劇照</span></figcaption></figure><p>之前看林嘉欣面書，甚麼兩個女孩的母親，患上乳癌，嚇了一跳。然後才知道她在述說《美國女孩》演的角色。再想想，其實沒甚麼大驚小怪，活在這個年代活到這些年歲，有關個人危疾、全球疫情、移民和回流以至家人散聚等這些事，總有一項我們必然頻繁面對。《美國女孩》好多鏡頭都讓人悽然，只是基礎的一家四口之相愛和傾軋，足以牽扯每個人內心的巨浪。尤其我們跟家人一起睡覺一起吃飯，迫於無奈齊上齊落，會積累深厚而不可取代的牽繫，同時間看似瑣碎卻密集的日常碰撞，足以消磨血緣親厚關係。我們未必願意承認心底裡的利己心態，面對家人更好像奉旨接收遷就和愛，當所謂親情跟個人利益發生衝突，可以反臉得最無義，下殺手往往最重最狠。</p>

<p>不少人談論剛奪金馬新人的大女兒方郁婷的演出驚為天人，她拍攝《美國女孩》時年僅十四歲，這樣的少年素人背景在近年幾乎所向披靡。尤其方郁婷的表現是怪物級完美，她愛媽媽更愛自己，五年來的美國生活已經建立不能磨滅的青春構成，回到台灣她頓時失去了大半魂魄。她逐漸長大成為一個自主個體，她考慮自己的愈來愈多，她對母親的關懷跟自己的心中嚮往不斷爭鬥。她關心母親病情，但如果可以選擇，她隨時可以離開這個家庭遠去。她內心一直在爭持，可以這邊廂一起歡天喜地吃雪榚，那邊廂可以咀咒患癌的母親。她的演出深層次地兼備反叛和怨懟。她在馬糟企圖馴馬一幕，拍出那奇幻童話感，不知道為甚麼會想起李安，好夢幻文藝也讓人不寒而慄。演妹妹的林品彤也相當厲害，她靈動自如，開始懂事又未長出很強橫的羽翼，她滿心不忿卻仍會思考父母離異和媽媽的健康，她是家庭的一個緩衝。</p>

<p>其實想說林嘉欣處境更不容易，在這個流行明星輸給素人的時代，她那張熟悉的美臉是個重大負擔，面對兩位天才型小演員，她竟然可以演活患癌母親的凄酸。她本來比女兒更愛西方生活，可是命運要她走回一條殘破舊路。她看著女兒也看著自己的少年，她明白她爭取的自由卻迫不得已施予束縛。看她跟女兒吵架，然後丈夫介入下更重手，她於心不忍又努力勸阻，通常媽媽都是這樣又暴力又仁慈。又看她接受化療一幕，竟然感受到她的口是苦的，不像在演，像真正注射藥物後的反應。切除乳房後，她在丈夫面前換衣服先關上燈，失去具性徵的器官，形成了夫婦間觸手卻不可及的暗黑疏離，林嘉欣舉手投足就製造了跟丈夫跟女兒的無形隔膜，她的確是好頂尖的演員，她有能力讓我們去憐惜那個角色。演丈夫的莊凱勛，增肥削薄頭髮後有點像朱江，好喜歡這角色沒有搞外遇的典型設定，他只是很平常地為生活營役不修邊幅，他只是平常地欠缺勇氣去撫摸那道傷痕。</p>

<p>一個演員厲害是演員自己厲害，如果所有演員厲害就是導演厲害。首度拍長片的阮鳯儀非常細膩動人，即使寂靜的畫面也有奇妙的張力，她準確洞悉每個人的自我和立場，即使面對至親，個人的慾望和感受仍是不容侵犯。故事中沒有人犯錯，只是生活每每讓每個人心目中的理想磨滅，我們對將來不再抱有期望，我們口說去愛卻付不出耐性，忍受不了就向身邊就近的人埋手。忽然會想起如果阮鳯儀拍張愛玲會是怎樣的光景，她好像更明白親人之間的腥風血雨，更大膽點出家庭關係暗藏創傷和悲痛，相處就是艱難。彼此間如果沒有透徹理解和退讓，再深重的血緣也要面對殘忍考驗。既蒼涼也真實！</p>

<p>&nbsp;</p>

<p>原刊於<a href="https://www.facebook.com/kamshing.hk/posts/2066594180154592" target="_blank">作者 Facebook</a></p>]]></content>
        <author>
            <name>金成</name>
            <uri>https://www.thestandnews.com/author/%E9%87%91%E6%88%90</uri>
        </author>
        <category label="文化"/>
        <published>2021-12-08T09:49:12.959Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[梅艷芳]]></title>
        <id>https://www.thestandnews.com/culture/ab_%E6%A2%85%E8%89%B7%E8%8A%B3</id>
        <link href="https://www.thestandnews.com/culture/ab_%E6%A2%85%E8%89%B7%E8%8A%B3"/>
        <updated>2021-11-16T04:55:45.185Z</updated>
        <summary type="html"><![CDATA[今年六月看了《梅艷芳》，聲音還未完全配好的。之前沒有看好，人物傳記電影可以有一百萬樣把電影拍得差勁的可能，尤其主角是梅艷芳，尤其她身邊永遠有一位張國榮。這麼近代這麼仍受廣大樂迷熱切嚮往的她，女主角唔似或太刻意扮都死，太歌功頌德或太過份描畫舞台王者也太理所當然。自己其實羞愧，少年傾慕過她的歌藝，貪圖色相轉投葉蒨文懷抱，再過兩年又戀上王菲。好快就覺得梅艷芳是上代人。如果有機會見到她會跟大隊叫聲梅姐，…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/1_LwRfm_Dwu01a8.png"><figcaption><span>電影《胭脂扣》劇照</span></figcaption></figure><p>今年六月看了《梅艷芳》，聲音還未完全配好的。之前沒有看好，人物傳記電影可以有一百萬樣把電影拍得差勁的可能，尤其主角是梅艷芳，尤其她身邊永遠有一位張國榮。這麼近代這麼仍受廣大樂迷熱切嚮往的她，女主角唔似或太刻意扮都死，太歌功頌德或太過份描畫舞台王者也太理所當然。自己其實羞愧，少年傾慕過她的歌藝，貪圖色相轉投葉蒨文懷抱，再過兩年又戀上王菲。好快就覺得梅艷芳是上代人。如果有機會見到她會跟大隊叫聲梅姐，心底𥚃著實失去追隨她後期作品的熱愛。</p>

<p>看著她愈來愈瘦，本來稱不上漂亮的她更瘦骨嶙峋。直到她步張國榮後塵離去，沒有太大悲慟，只想到死亡是會傳染的。直至她逝世十周年那個由張學友領軍的「10思．念演唱會」，比起同年那個悼念張國榮的好看太多。再陸續聽回好多她當年好好聽也好有型的歌，又翻看她從前演過的電影，《胭脂扣》、《鍾無艷》、《男歌女唱》及《男人四十》甚至跟周星馳拍的《審死官》和《龍過雞年》，想起她在演戲原來也有這麼多面，才由頭至尾重新嚮往她的才華和厲害。只有她在歌、影兩方面的成就不下於張國榮。</p>

<p>梅艷芳當年參加新秀，好像就算由小孩子做裁判，也會知道這位姐姐必贏的，就如當年選港姐看到朱玲玲李嘉欣一樣，勝負相當明顯。記得她初出道像熟了的日本歌手，留了中森明菜頭卻好不明菜，到〈赤的疑惑〉剪了頭短髮，更有年輕太太味。直至〈夢幻的擁抱〉超有型的中性形象，西裝外披搭長褸，開始了她的百變形象。那時候每次等歌手出碟，在電台只聽到音樂，只能在電視看到梅艷芳每次出碟的熱爆造型，和每次為歌曲訂製的舞步。某程度她從來沒賣sex appeal，即使後期的〈烈焰紅唇〉也只是嘩嘩嘩，終於驗證她沒有紋身。但梅艷芳，沒需要賣性感，她的歌聲她在舞台上的氣勢就足夠讓人俯首稱臣。但梅艷芳也承認，自己沒斷續的負面新聞，男友如走馬燈、大花筒、家庭糾紛等，一直在抵消她的聲勢。對了她即使再紅，沒有覺得她好幸福。她贏了好多男人，最後卻沒得到想要的男人。</p>

<p>梅艷芳在1982年贏新秀冠軍出道，只比陳慧嫻早兩年，但她一出道已經是大姐大格。只一年時間，由〈心債〉到〈赤的疑惑〉，她展現了寬廣而饒富趣味的嗓子，她出道時被認為跟蔡琴是並駕齊驅的低音皇后，既俊朗也醇厚，力量澎湃洶湧足以懾服雄性，但她的腔口奇妙地在轉角位置帶上幾分嬌媚稚氣，後來她唱〈I Q博士〉示範得好清楚，徐小鳳唱不到的。出道作〈心債〉，好柔情好磁，跟當年剛走紅的黃日華和初演劇集的梁朝偉有如旭日初升，一切都是向好的。再看看電影《梅艷芳》會明白，這位香港女兒的傳奇，也就是香港最輝煌的八十年代歷史側記。她的起和跌，幾乎跟香港的遭遇同步。</p>

<p>由梁樂民執導是始料不及，他之前跟陸劍青拍《寒戰》，總算創作了男主角的激烈嗌交戲，但由他單人拍《梅艷芳》非常摸不著頭腦。但正正他沒有太強作者論，連同老闆江志強都只是一心一意為梅艷芳流傳後世，他們就努力去找對的人去演，找優秀的班底去重塑梅艷芳和我們一起成長的時代景象。電影特技不只還原度高，而且好真摯動人，看看尖東海傍，佐敦裕華和妙麗中心，街上繁華紛亂的小販檔，還有那個華麗標誌利舞台，讓我們重新感受當年的情懷和志氣。自己特別喜歡幾個王丹妮在人來人往的街道行走的鏡頭，從前的香港已經是這麼繁忙擠擁的，但步履還是充滿朝氣滿懷希望的，那情懷不是虛構！</p>

<p>電影幾近以直線鋪排，從荔園開始了她和姐姐梅愛芳的姊妹情，近乎是電影的主軸。廖子妤的優秀演出，對王丹妮扶持作用非常巨大，她演出了天意弄人的悲傷感。加插兩段梅艷芳算是廣為人知的感情戲，都比想像中來得戲劇化。前段和近藤真彥的一段情，有點過長和渲染，削弱了劉俊謙演張國榮的鋪排。第二段情補寫了梅艷芳被掌摑而逃亡的情節，沒想到電影會加插這段，細節的真確性不談，效果好震撼，某程度也是梅艷芳的轉捩點。王丹妮高大漂亮，甚至後半部加插大量梅艷芳真人片段時，會映照王丹妮稍為的過份艷麗。但她好努力也天分高，她好準確地模仿梅艶芳七分的說話腔調和豪爽，反而更不著痕跡地演活天后。所有演員也配合得宜，古天樂演劉培基，好像是對著梅艷芳本人來演的。</p>

<p>科技也大大補助王丹妮唱歌的說服力，導演梁樂民說過電影的混音技術，是參照早前Queen的傳記片《Bohemian Rhapsody》，用幾把聲音交疊混雜，好聰明也好動人。王丹妮一開聲，眼淚就湧上心頭。如果只有梅艷芳原聲，會太咪嘴太代唱，重點是既一定要她存在，再加插王丹妮自己和其他歌聲，奪取那如幻似真的神髓，借屍還魂的感覺才來了。一聽她錄〈心債〉就毛管戙，好像是好久沒見的朋友或親人上來重聚，也見證當年梅艷芳初出道的驚世才華。還有最好看一段，是梅艷芳在未出唱片之際，經理人來電求救，某個集合港、日、台語的麻煩友的夜場，因為歌手臨時失場，需要梅艷芳頂上，一鏡過拍她如何一人唱盡三種語言，一個其實有十五年舞台經驗的新人，把現場鎭壓得貼貼服服。這場演得揮灑自如的王丹妮有上身感覺，比打後紅館演出更驚為天人。某程度說，這場也好像預告了她日後招惹江湖的宿命。</p>

<p>&nbsp;</p>

<p><a href="https://www.facebook.com/kamshing.hk">作者 facebook page</a></p>]]></content>
        <author>
            <name>金成</name>
            <uri>https://www.thestandnews.com/author/%E9%87%91%E6%88%90</uri>
        </author>
        <category label="文化"/>
        <published>2021-10-31T11:36:54.610Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[休止突然]]></title>
        <id>https://www.thestandnews.com/personal/%E4%BC%91%E6%AD%A2%E7%AA%81%E7%84%B6</id>
        <link href="https://www.thestandnews.com/personal/%E4%BC%91%E6%AD%A2%E7%AA%81%E7%84%B6"/>
        <updated>2021-10-09T14:08:04.705Z</updated>
        <summary type="html"><![CDATA[因為成為 Asia’s 50 Best Restaurants 第一位，要吃大班樓每次訂枱至少要等待三至四個月，一直又稀罕又沒有興致排長龍。好幸運今年六月天命請我去吃，當時已經立即再訂枱，午餐大概也要來到中秋。時間好奇妙的，有時過得好快，這三個月卻有點慢，應該說這三個月香港所發生的事情密集而荒誕。沒想到等待多吃一次大班樓的日子，世界變了很多。當日同枱有人已經離港，又是無聲無色的。就像小時候九七前…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/DecQkEOV4AAajZE.png"><figcaption><span>圖片素材來源：Anthony Bourdain Twitter</span></figcaption></figure><p>因為成為 Asia’s 50 Best Restaurants 第一位，要吃大班樓每次訂枱至少要等待三至四個月，一直又稀罕又沒有興致排長龍。好幸運今年六月天命請我去吃，當時已經立即再訂枱，午餐大概也要來到中秋。時間好奇妙的，有時過得好快，這三個月卻有點慢，應該說這三個月香港所發生的事情密集而荒誕。沒想到等待多吃一次大班樓的日子，世界變了很多。當日同枱有人已經離港，又是無聲無色的。就像小時候九七前，忽然失去了好多感情，真沒想到幾十年以後，發生另一次更大規模，更沒有轉圜餘地。有時會對離開的人產生微妙的恨意，會懊惱他們拋下自己，也拋下香港。更有時，好想隨便找個地方，自己飾演離開那個，即使再陌生即使再恐懼，也希望試試自己成為去拋下別人那一位。表面看來有點像王家衛一直崇尚的，為了不想被人拒絕，便預先拒絕所有人。</p>

<p>遭逢大變的年代，有些人或自己，會忽然露出潛藏在內心的另一個自己，不是成為甚麼壞分子，而是那個一直跟你在心內爭辯的人。身邊有些人和事物的確過了賞味期限，剛剛五十歲，沒時間再拖拉，真正全方位活得好疲憊，好希望把大部分習以為常的陋習改變，寧願去適應從來沒有適應過的。如長久以來的工作，居住的環境，其實想拋棄的，更包括自己。想起一位要好朋友，我們二十年來幾乎無所不談，彼此幾乎知道所有私密，由於沒有任何工作關係，沒有利益衝突也沒有金錢往來，好喜歡他的為人和才藝，感情一直純厚真摯。整天侃侃而談歡天喜地的他，說過隨時準備死亡，最離奇是，我相信他是認真會做這件事的人。有次晚飯，好像是我隨便猜測一些微不足道的事情，便惹來他好認真的反駁，然後大家各持己見，記憶我們從來沒有這麼爭吵過。其實過程不算激烈，但大家忽然好像醒覺了，看得好清楚彼此價值觀的差異。太平盛世大家都沒需要探討那裂紋，是到了某時某刻，突然大家都不想忍耐了，內心那另一個我愈來愈強大，丁點兒分歧足以嫌棄大家，甚至想在生命中抹掉了彼此出現過的痕跡。而所有事情可以發生在一頓飯，一個電話對談。</p>

<p>朋友讓我想起 Anthony Bourdain。多年前偶然在明珠台看過他的節目，對他的串嘴有型印象至深。生於 1958 年的 Anthony Bourdain，少年時便接觸毒品，直到二十歲才成功戒除毒癮。後來他成為一名廚師、作家及節目主持人。波登（中譯）五官分明，生來一副明星相，略纖瘦但看來壯健的骨架穿衣很好看。他也好懂戴表，看過他戴古董 Rolex 和響鬧積家，古典和運動型都難不到他。波登身高 193 cm，跟我們的 193 同樣有張得勢不饒人的串嘴，更受注目是他一手辛辣文字。有些人行出來就發光亮，難得在波登身家富有卻非常貼地，他經常不設防地邊享受美食，邊揭露飲食行業不為人知的黑暗面，往往引申至移民政策、勞工狀況、種族主義等重大社會議題。他本來可以窮奢極侈的，他沒有；本來他可以側側膊在上流社會享盡富貴活下去的，<a href="https://www.thestandnews.com/international/%E7%BE%8E%E5%9C%8B%E6%98%8E%E6%98%9F%E5%BB%9A%E5%B8%AB-anthony-bourdain-%E8%87%AA%E6%AE%BA%E8%BA%AB%E4%BA%A1" target="_blank">偏偏他在 2018 年毫無先兆下自殺身亡</a>，享年僅 61 歲。</p>

<p>波登十歲時前往法國探親，途中品嚐過漁夫給他一口生蠔，充滿鹽水與鮮滑，令他矢志成為要享受世界的美食家。後來他在 Culinary Institute of America 學廚，在不同餐廳當廚師。但讓他聲名大噪始終是他的寫作和刻薄幽默的旅遊美食節目，包括《名廚吃四方》、《波登不設限》、《波登過境》及最近期於美國《有線電視新聞網》（CNN）播出的《波登闖異地》。有些人總是一開聲就引人注目，波登因為熟悉一切廚房運作，他初成為作家前，就寫過名言「星期一不要吃魚！」，揭露紐約所謂一線食店的廚房背後種種不當操守，幾乎摧毀了該地所有食店逢星期一的海鮮銷售。</p>

<p>波登對世界充滿好奇和道德批判。他在節目中廣泛結識不同階層的朋友，除了吃盡天下不同的食物，也藉著節目探索人生百態，體察各階層人物是如何生活。他的內心謙卑又尖鋭，他可以吃最名貴最高檔的，對低下層的薯條漢堡包也尊崇有加。節目最好看是他開宗明義痛恨偽善和作狀，他可以坦誠展現落後地區的市集內，那些滿布蒼蠅的食物原材料畫面。在鏡頭前，他從不忌諱粗言穢語，吃相也毫無修飾，可以在北非吃羊睾丸、在墨西哥吃蟻蛋，以及在越南生吞眼鏡蛇的血肉內臟。他在《紐約客》文章的開首寫道：「優質的食物，往往跟血和器官、殘忍和腐壞有關。」這些表態為他明確訂下風格調子。他對越南菜推崇備至，最經典鏡頭是他跟前美國總統奧巴馬，在河內基層食店吃最地道最街坊的豬肉米線和炸春卷，連同啤酒共消費六美元。</p>

<p>全世界的美食節目也只會吹捧大廚，波登卻專門嘲笑名廚，反而為幫廚甚至洗碗工人發聲。他的《廚房機密檔案》（Kitchen Confidential: Adventures in the Culinary Underbelly），就鉅細無遺地展露餐飲業背後的光怪陸離。波登對台灣的形容也坦白準確，他認為台灣不夠漂亮，卻擁有豐富的內涵，更斷定小籠包是呈現豬肉鮮味的最佳載體。波登也曾在香港拍過美食節目，造訪我們熟悉的北角東寶海鮮大排檔。他跟杜可風又是老朋友，對杜式拍攝風格神醉。題外話，如果你聽過杜可風對香港的形容，會確信這醉酒鬼佬比好多香港人更深愛香港，沒有把這裡當造搵食天堂。那當然，波登跟香港人最共通處是他超愛日本的一切，他說過如果可以，他願意死在東京。</p>

<p>波登漸老，滿頭濃密鬈髪變得斑白。他愈來愈認為生活充滿微妙和複雜，總之好不痛快。他說年紀愈大，沒再相信任何事實，只相信懷疑。他的自殺，也惹起不少懷疑，安東尼波登過去曾有過兩段婚姻，分別在 1985 年、2007 年，皆離婚收場，直到 2017 年大方認愛意大利女星 metoo 運動先鋒 Asia Argento，二人相差十九歲，本來一直恩愛甜蜜。不過外媒爆料，波登臨終前在法國錄製節目時，身在羅馬的 Asia Argento 被拍到和一名男記者牽手相擁，而波登死前兩小時，在社交媒體上載印有「Fu*k Everyone!」字眼 Tee，沒多久便刪文，引發無限遐想。也有傳聞 Anthony Bourdain 工作太密集頻繁，兼顧不了質素讓他飽受煎熬。他曾漫不經心吐露自己看來飛揚跋扈的另一面心底話：「I understand there is a guy inside me who wants to lay in bed, smoke weed all day, and watch cartoons and old movies. My whole life is a series of stratagems to avoid, and outwit that guy!」就是有一天，心底裡另一個朋友反勝過來，他說了停，便把一切休止，可以好突然！</p>

<div><figure><img src="https://assets.thestandnews.com/media/photos/7040608355557919709.jpg"><figcaption><span>作者 Facebook 圖片</span></figcaption></figure></div>

<p>&nbsp;</p>

<p>原刊於<a href="https://www.facebook.com/kamshing.hk/posts/2020848264729184" target="_blank">作者 Facebook</a></p>]]></content>
        <author>
            <name>金成</name>
            <uri>https://www.thestandnews.com/author/%E9%87%91%E6%88%90</uri>
        </author>
        <category label="生活"/>
        <published>2021-10-09T13:51:17.668Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[有些人總是被忘記]]></title>
        <id>https://www.thestandnews.com/culture/%E6%9C%89%E4%BA%9B%E4%BA%BA%E7%B8%BD%E6%98%AF%E8%A2%AB%E5%BF%98%E8%A8%98</id>
        <link href="https://www.thestandnews.com/culture/%E6%9C%89%E4%BA%9B%E4%BA%BA%E7%B8%BD%E6%98%AF%E8%A2%AB%E5%BF%98%E8%A8%98"/>
        <updated>2021-07-31T05:08:31.931Z</updated>
        <summary type="html"><![CDATA[在電影中，蘇媽沒說錯，兒子出征最後一次奧運後，的確沒有人再會記得蘇樺偉。至少在平日，我們不會無端端想起他。他那句最動人的「行得比人慢，但跑得比人快」的座右銘，就在拍那一次廣告被消耗掉。打後他離開賽道，他再如何克服痙攣而繼續為人生奮鬥，如何應對日常生活難以避免的歧視，應該沒有太多人會關心和理會。甚至張家朗奪取的奧運金牌，是繼李麗珊的第二枚。蘇樺偉那枚來自殘奧的，沒有計算在內。群眾大概會認為，蘇樺偉…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/gypXONoeFzjaHg.jpg"><figcaption><span>電影《媽媽的神奇小子》劇照</span></figcaption></figure><p>在電影中，蘇媽沒說錯，兒子出征最後一次奧運後，的確沒有人再會記得蘇樺偉。至少在平日，我們不會無端端想起他。他那句最動人的「行得比人慢，但跑得比人快」的座右銘，就在拍那一次廣告被消耗掉。打後他離開賽道，他再如何克服痙攣而繼續為人生奮鬥，如何應對日常生活難以避免的歧視，應該沒有太多人會關心和理會。甚至張家朗奪取的奧運金牌，是繼李麗珊的第二枚。蘇樺偉那枚來自殘奧的，沒有計算在內。</p>

<p>群眾大概會認為，蘇樺偉比好多同輩幸運，至少他有劉德華關顧。卻不容易想像運動員會生活艱難到這個程度，蘇樺偉每月只得到政府三千元津貼。《媽媽的神奇小子》中描述蘇樺偉曾經因為經濟問題而放棄練習跑步，甚至需要為了七千元月薪做速遞工作。不知道情節之真確性，但我們見過不少國家級運動員，就算拿過幾個獎牌，時候到了光環消失無蹤，最終退役時也可以淪落街頭賣藝。作為蘇媽，明知自己孩子先天各項缺陷，努力讓他仍在巔峰期多賺錢，為不可知的未來盡量儲藏豐厚資本也無可厚非。只是作為父母，先要拿出勇氣坦誠直指兒子的天生缺陷，要有足夠的殘忍和愛。</p>

<p>飾演幾個不同階段的蘇樺偉都非常出色。少年版馮皓揚出場有些驚為天人，會以為他們真的找來痙攣的演員來演。有些捨不得他變成了梁仲恆，雖然梁演得同樣優秀。蘇樺偉真人有些孩子臉，介乎兩人之間。而馮皓揚臉容童稚，身體有點孱弱，好易被欺凌的樣子，跑起來歪歪斜斜的真實感好強，更得我心。相對梁仲恆比較高大，他演和跑都比較專業，不諱言他樣子比較成熟，跟吳君如當母子不算太相襯。</p>

<p>吳君如也奇怪的，她在電影中愈演愈年輕，始終欠了那含辛茹苦的滄桑。電影部分拍得比較煽情在所難免，有時現實世界的不幸和難堪的確可以比戲劇更戲劇，會慶幸這陣子香港電影的類型和題材都寬闊了，《神奇小子》幾好看。只是要拍這麼一齣悲天憫人的故事，總覺得演技需要更多微妙的醞釀，尤其夾雜在真實感強大的素人之中，太老練的演員反而容易失色。所以我更想說張繼聰演得好，他好認真，看到他的樣子，我沒覺得好笑。</p>

<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/uo5yR2w8Z04" title="YouTube video player" width="560"></iframe></p>

<p>&nbsp;</p>

<p><a href="https://www.facebook.com/kamshing.hk/posts/1965811596899518" target="_blank">作者 Facebook</a></p>]]></content>
        <author>
            <name>金成</name>
            <uri>https://www.thestandnews.com/author/%E9%87%91%E6%88%90</uri>
        </author>
        <category label="文化"/>
        <published>2021-07-31T05:06:23.603Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[他緩和了尷尬]]></title>
        <id>https://www.thestandnews.com/society/%E4%BB%96%E7%B7%A9%E5%92%8C%E4%BA%86%E5%B0%B7%E5%B0%AC</id>
        <link href="https://www.thestandnews.com/society/%E4%BB%96%E7%B7%A9%E5%92%8C%E4%BA%86%E5%B0%B7%E5%B0%AC"/>
        <updated>2021-07-11T08:13:38.121Z</updated>
        <summary type="html"><![CDATA[也許連谷德昭自己未必知道，他的《納米無名火》除了暖透納米樓住客，也鼓舞海量大叔，原來只要努力做，就真係有得做。今時今日改朝換代是恐怖地不留情面，淘汰速度永遠快過轉型速度。各行各業得番半條人命仲隨時被指導：「點解唔試下轉型啫！」啲人係通常好離地。其實一直有轉，只不過上年跟住2020年個trend轉，轉到而家至轉型，而家又話2021唔係咁，2022又會點會點！無得跟！轉唔切。又或者有時是timing…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/20210711-12_yCy2bo1.png"><figcaption><span>圖片素材來源：《納米無名火》截圖</span></figcaption></figure><p>也許連谷德昭自己未必知道，他的《納米無名火》除了暖透納米樓住客，也鼓舞海量大叔，原來只要努力做，就真係有得做。今時今日改朝換代是恐怖地不留情面，淘汰速度永遠快過轉型速度。各行各業得番半條人命仲隨時被指導：「點解唔試下轉型啫！」啲人係通常好離地。其實一直有轉，只不過上年跟住2020年個trend轉，轉到而家至轉型，而家又話2021唔係咁，2022又會點會點！無得跟！轉唔切。又或者有時是timing，轉早轉快少少亦可壯烈犧牲。去年六月首度用姜先生做封面，問極都沒有高貴客戶參與，甚麼好MK不夠優雅。結果那個月的刊物兩日售清少少哄動，但該月虧蝕相當甘，值得畢生難忘。</p>

<p>有時會認為，而家三十零歲已經可以好尷尬可以好不知所措。明明二十歲時開始努力拚搏，打好十年基礎應該係時候衝上一至兩層樓。但非常忽然之間，三十幾歲站在二十幾歲旁邊，可以幾尷尬曖昧，那個外形那個妝容那個談吐那個呼吸空氣的方式，都似乎截然不同。香港好像忽然爆出了一個全新時空，形成了一塊平衡陸地。在街上好多人忽然看來蒼老，出席活動的明星光譜急速轉變顏色。早兩年來勢洶洶的，忽然感覺變成了上兩代人。本來因爲各種不公不正而形成的世代之爭，更演化為世代撕裂，撕裂範圍極微細，十幾歲同六十歲撕，同四十幾歲撕、同三十幾或廿幾都可以撕，撕裂得片片碎，不合時宜每天都在更新定義。更加還有下一句：五十幾再打後反而沒壓力沒尷尬，因為事實老早沒有人看到你存在，淡白的、透明的，連參與撕裂的資格也未必有。</p>

<p>Mirror、Error之出現一瞬間改變了規則改變了狀態。又或者更廣義今年爆紅的好幾位，沒有刻意但夾手夾腳地形成了一條幾明確亦殘忍的分水嶺，他們不是在冒升，他們是一下子佔領大半山頭，重新製造歡欣和熱鬧。不只上兩代面目模糊，就只是分水嶺前剛剛嶄露頭角的，跟之後出現的已經是兩個世界。如果之前沒有好好站穩一個位置，會突如其來下降了兩格、寂靜了一點五倍、褪色了三至五級。過往十年的掙扎奮鬥眨眼間徒勞無功。抵死在，即使一直躲避Mirror，不想靠他們寫字，怕生埗怕唔襯亦怕叨光。一齣《大叔的愛》，好彩大叔代表黃德斌夠爭氣超搶眼，也不能詐作看不見Anson 和Edan，尤其凌少牧那一顰一笑一抿嘴「食嘢喇！」「間屋我執啦！」那含辛茹苦又滿不在乎，簡直滋陰補腎，那眼妝那表情那演技，近乎無需千錘百煉，好像只要行埋位，世界是你們的，你們演，就好看就甜美。事到如今只能努力不隨大隊，未必一定要下下尖叫，但愈來愈自然投入關注阿Jer和凌少牧。還有Stanley 都唔錯。</p>

<p>其實最想說，《納米無名火》更動人肺腑，在這個年代，五十幾歲的谷德昭溫柔體貼，誠心誠意去做好件事，去明白這個社會，去洞察各個階層，去關心納米樓住戶，去明瞭他們心聲，去煮個靚菜一起吃。舉手投足間惹人感觸又幽默風趣，沒有絲毫賣弄，看得清楚谷德昭對年輕人，對市民生活有幾大好奇有幾真心關懷。谷導的好看，給予被撕裂階層難以形容的巨大鼓舞，他的節目緊接上個時段的超熱劇集。他把五十幾歲可以古道熱腸，和二十幾歲可以顛倒眾生這兩件事，重新合情合理地連接起來，形成一個各領風騷的套餐，老中青再度同蓆收看電視，他緩和了那個尷尬，也稍稍撫平那道裂紋。他示範了憑藉自己節奏，五十幾仍可以有光明路途，如此類推！</p>

<p>&nbsp;</p>

<p>《納米無名火》部份宣傳片：</p>

<p><iframe allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowfullscreen="true" frameborder="0" height="591" scrolling="no" src="https://www.facebook.com/plugins/video.php?height=476&amp;href=https%3A%2F%2Fwww.facebook.com%2FViuTV.hk%2Fvideos%2F245554713709222%2F&amp;show_text=true&amp;width=476&amp;t=0" style="border:none;overflow:hidden" width="476"></iframe></p>

<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/V0-N13h2xiE" title="YouTube video player" width="560"></iframe></p>

<p><a href="https://www.facebook.com/kamshing.hk/photos/a.676097379204286/1953461991467812/">作者 facebook page</a></p>]]></content>
        <author>
            <name>金成</name>
            <uri>https://www.thestandnews.com/author/%E9%87%91%E6%88%90</uri>
        </author>
        <category label="社會"/>
        <published>2021-07-11T06:53:39.894Z</published>
    </entry>
</feed>