<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>https://www.thestandnews.com/echigo-tsumari/rss</id>
    <title>立場新聞 Stand News - 越後妻有</title>
    <updated>2021-12-06T23:06:22.343Z</updated>
    <generator>https://github.com/jpmonette/feed</generator>
    <link rel="alternate" href="https://www.thestandnews.com/echigo-tsumari"/>
    <subtitle>《立場新聞》編採獨立自主，不受任何贊助人、財團、權力機構及黨派左右。我們以非牟利原則營運，所有經營盈餘和贊助，只會用於傳媒事業。</subtitle>
    <rights>© 2021 立場新聞. All rights reserved.</rights>
    <entry>
        <title type="html"><![CDATA[【越後妻有 4】誰的改造？──藝術祭的波及效應]]></title>
        <id>https://www.thestandnews.com/art/%E8%AA%B0%E7%9A%84%E6%94%B9%E9%80%A0-%E8%97%9D%E8%A1%93%E7%A5%AD%E7%9A%84%E6%B3%A2%E5%8F%8A%E6%95%88%E6%87%89</id>
        <link href="https://www.thestandnews.com/art/%E8%AA%B0%E7%9A%84%E6%94%B9%E9%80%A0-%E8%97%9D%E8%A1%93%E7%A5%AD%E7%9A%84%E6%B3%A2%E5%8F%8A%E6%95%88%E6%87%89"/>
        <updated>2016-02-22T11:12:31.000Z</updated>
        <summary type="html"><![CDATA[空屋，因為藝術祭而翻新；人流，因為藝術祭而興旺。以藝術為名的社區改造，為越後妻有帶來變化，變化卻不止於當地，也延及每一個參觀者。誠然，到訪三年展的遊客，駐留時間不長。根據《大地の芸術祭 2012 総括報告書》，主辦單位抽樣訪問大約 2,000 名參觀者，結果發現他們一般逗留不多於三日兩夜，最常見者更是即日來回。短暫的藝術文化體驗，能夠在旅人心中造成怎麼樣的效應？它是否真能為觀眾帶來一場思維的更新…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/3_0mTfU.png"><figcaption><span>大地藝術祭 Ilya & Emilia Kabakov 作品《棚田》</span></figcaption></figure><p>空屋，因為藝術祭而翻新；人流，因為藝術祭而興旺。以藝術為名的社區改造，為越後妻有帶來變化，變化卻不止於當地，也延及每一個參觀者。</p>
<p>誠然，到訪三年展的遊客，駐留時間不長。根據<a href="http://www.city.tokamachi.niigata.jp/page/000028293.pdf" target="_blank">《大地の芸術祭 2012 総括報告書》</a>，主辦單位抽樣訪問大約 2,000 名參觀者，結果發現他們一般逗留不多於三日兩夜，最常見者更是即日來回。短暫的藝術文化體驗，能夠在旅人心中造成怎麼樣的效應？它是否真能為觀眾帶來一場思維的更新？抑或只是純粹的情感消費？</p>
<p>2012 年，歐洲創作組合 Elmgreen &amp; Dragset 製作的了一件名為《POWERLESS STRUCTURE, FIG. 429》的作品，將一些白色立方體歪歪倒倒地堆疊起來。立方體上面寫著「Museum」，具體地比喻將「白立方」(White Cube) 的當代藝術定義重組。作品放置於被視為越後妻有藝術祭縮影的里山現代美術館，彷彿為這場三年展定調──藝術，不限於堂廟。</p>
<p>熟悉視覺藝術評論的愛爾蘭學者 Brian O’Doherty 在其著作&nbsp;<em>Inside the White Cube: The Ideology of the Gallery Space&nbsp;</em>中指出，藝廊設計牆壁刷白，並以天花燈作為唯一的光源，是刻意模仿中世紀時期教堂的氣氛。乾淨、純白、無陰影地展示藝術品，將藝術提升至一種「永恆」的狀態，反映歐西文化自文藝復興以來，脫離宗教枷鎖，走向人文精神發展的源流。然而，這是越後妻有的藝術祭嗎？符合日本美學發展源流嗎？</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/5_oo90O.png"><figcaption><span>Elmgreen &amp; Dragset 的作品《POWERLESS STRUCTURE, FIG. 429》</span></figcaption></figure><p></p>
<p>「凡是由人類創造出來的東西都是美術。我認為其原始部分則是『生活藝術』。」藝術總監北川富朗在《北川富朗大地藝術祭：越後妻有三年展的10種創新思維》為藝術重新定義，並以越後妻有三年展作為示範實驗的場所。他認為所謂的「純藝術」(Fine Art) 也離不開生活，生活才是藝術發生的根本。</p>
<p>文章又以繪畫和舞蹈為例，指出「日本自古以來，許多名畫便是畫在紙門、窗戶、天花板上」，又言「這裡的舞蹈就是祭典，只有被選上的人才能上舞台，但祭典並非是觀眾得畢恭敬觀賞的東西，而是生活的一部分。」</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/2_NMtJn.png"><figcaption><span>用藝術翻新老屋的《蛻皮之家》</span></figcaption></figure><p></p>
<p>翻開藝術祭的地圖，從田野間的裝置，如 Ilya &amp; Emilia Kabakov 的《棚田》；到用藝術翻新老屋的《蛻皮之家》；還是在剛剛被殺的奴奈川小學舉行的「大地藝術祭學期 2015」，將於 8 月1 至 7 日期間「重新開課」，傳授關於藝術祭、美術、體育、家政、舞蹈的知識，所包涵的領域，從工藝到烹飪都有，無一不從生活出發，可見北川對於「藝術」的定義已經拉闊至人類創作，無怪他曾言：</p>
<blockquote>
<p>「絕不僅只單純的活著而已，還包括概念式的生活。就像祭典、情書以及每天必須吃飯一樣，是日常生活中不可或缺的東西……美術就這麼簡單，生活的一切離不開美術，日常生活盡在美術中。」</p>
</blockquote>
<p>受西方美學影響多年的我們，或者習慣藝術發生在美術館、大堂會、歌劇院等堂廟之內。面對有如越後妻有藝術祭的觀點時，我們會選擇去鄙視那些「創作」談不上「藝術」；還是願意換一副眼鏡，用他們的視角，嘗試去理解不同思維模式？這個問題視乎出行者的心態。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/1_h0ne9.png"><figcaption><span>關於「大地藝術祭學期 2015」的詳細介紹</span></figcaption></figure><p></p>
<p>出遊可以是純粹消費，也可能存在逃避現實的原因，但社會學家 Erik Cohen 提出了其他旅遊模式的可能。他從旅人的主體性出發，將旅遊經驗劃分成五種模式：娛樂、轉移、經驗、實驗和存在主義。</p>
<p>娛樂模式，即是簡單的消費，享樂為尚的遊客；轉移模式則指人們透過空間的改變，獲得逃逸的舒坦感覺。經驗模式的旅人，雖然願意開放接收新文化，但仍然以本土為核心，希望吸收外地經驗，回去到家鄉進行改善工程。若旅人對本土文化的不滿更強，只希望不斷在其他文化體驗中尋找代替品，Cohen 稱之為「實驗模式」。</p>
<p>Cohen 提出的第五種模式是「存在主義模式」(Existential model)，旅人身處異國時，在保持自我和開放接受之間，取得主客定位的平衡。這種旅人抱持類近於流放者的心態，藉著離開熟悉的地方，脫離本土社會和文化背景，全情投入一種新的文化環境，並渴望成為其中一份子。他們放棄自身文化背景為中心的視角，持續地以最大的開放程度，嘗試融入異地文化。如若參觀者抱存「存在主義模式」心態到訪越後妻有三年展，藝術作為推動社區改造的力量，或者不止於當地，更可以延展至區外人士對藝術定義的再思考。</p>
<p>日本文學家川端康成的小說《雪國》因以越後妻有一帶的溫泉區為背景，而常被傳媒引用於藝術祭相關的報道。書中，男主角島村來自東京，在越後湯澤邂逅藝妓駒子。然而，無論他怎樣消費，最終也無法完全擁有純潔的駒子；渴望離開鄉鎮的葉子，向島村提出一同回東京的請求，卻在成行之前，在大火中死去，彷彿象徵小鎮的美好總是無法帶走。若以此為喻，城市人與鄉鎮之間的互相追慕，到頭來也只是一場徒勞。</p>
<p>徒勞，源於執著；放開，才能承受突破。你我或者都清楚，唯有盡量忘我，才可以在異地獲得改造和再生。想易行難，從石屎森林到大地藝術祭，你可曾預算自我覺醒的理性，能夠持續多久多遠？</p>
<p>&nbsp;</p>
<p>文／grace</p>
]]></content>
        <author>
            <name>立場報道</name>
            <uri>https://www.thestandnews.com/author/standnewsreport</uri>
        </author>
        <category label="藝術"/>
        <published>2015-07-28T13:02:03.000Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[【越後妻有 3】誰的參與？──藝術是唯一的方法？]]></title>
        <id>https://www.thestandnews.com/art/%E8%AA%B0%E7%9A%84%E5%8F%83%E8%88%87-%E8%97%9D%E8%A1%93%E6%98%AF%E5%94%AF%E4%B8%80%E7%9A%84%E6%96%B9%E6%B3%95</id>
        <link href="https://www.thestandnews.com/art/%E8%AA%B0%E7%9A%84%E5%8F%83%E8%88%87-%E8%97%9D%E8%A1%93%E6%98%AF%E5%94%AF%E4%B8%80%E7%9A%84%E6%96%B9%E6%B3%95"/>
        <updated>2016-02-22T11:12:25.000Z</updated>
        <summary type="html"><![CDATA[藝術三年展既以越後妻有為名，當中又包含幾多越後妻有之實？當地素材、題材雖然融入外來藝術家的作品之中，但當地人參與程度又有多高？誠然，當地人並不冷眼旁觀藝術祭的發生，從讓出房屋、借出農地的情況，可見他們是讓藝術遍地開花的關鍵。然而，越後妻有沒有本地藝術家？他們又有沒有在三年展中進行創作？──答案居然是少得可憐。最近三屆參展的當地藝術家人數，無一突破雙位。根據越後妻有三年展的官方數字，2009 年第…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/ch3-1_AFvrd.png"><figcaption><span>蔡國強今年為越後妻有三年展新作的《蓬萊山》</span></figcaption></figure><p>藝術三年展既以越後妻有為名，當中又包含幾多越後妻有之實？當地素材、題材雖然融入外來藝術家的作品之中，但當地人參與程度又有多高？</p>
<p>誠然，當地人並不冷眼旁觀藝術祭的發生，從讓出房屋、借出農地的情況，可見他們是讓藝術遍地開花的關鍵。然而，越後妻有沒有本地藝術家？他們又有沒有在三年展中進行創作？</p>
<p>──答案居然是少得可憐。</p>
<p>最近三屆參展的當地藝術家人數，無一突破雙位。根據越後妻有三年展的官方數字，2009 年第四屆及 2012 年第五屆參展本地藝術家數目，均為三個單位（個人或團體）。今年參展單位共有 315 個之多，但本地藝術家更跌至只有兩個──十日町 RC Club 和芹川智一。</p>
<p>十日町 RC Club 實為當地的模型飛機學會，以 2:1 比例製作模型，並舉行飛行比賽。自 2012 年起參與藝術祭，他們將於今年 8 月 23 日在田中飛行場舉行為時半日的「放飛機」表演，歡迎遊人免費觀賞。</p>
<p>而芹川智一則非土生土長的越後妻有人士，而是 1997 年從東京遷入。他自 2006 年起在下条築起木材營地，現在已經完成六座類似牛棚的建築。一系列名為《田園中的異國ing》的作品，不但開放予遊人參觀，亦可作為留宿住處。</p>
<p>走筆至此，記者不禁令人聯想到後殖民理論家 Gayatri Spivak「弱勢能否發聲？」(Can the Subaltern Speak?) 提出的憂慮：身份無法依靠他人代書，而是個人的覺醒。這篇發表於 1988 年的論文，向印度學術界的「下層民間研究」 (Subaltern Studies) 提出質疑。她認為接受殖民教育出身的知識份子，書寫弱勢社群的經歷，無法做到真正的「弱勢發聲」。</p>
<p>同一道理置於越後妻有亦然。跟東京大阪的大都會相比，當地還是存在一道城鄉差距的界線，更不用說隔著文化差異的海外藝術家。願意投放時間和心力，真正融入當地、體驗生活的藝術家不多，名家大師尤其不常在此。</p>
<p>記者團到訪越後妻有期間，大陸同業問：蔡國強在嗎？台灣同業問：幾米在嗎？──兩道問題的答案都是否定的。事實上，這些赫赫有名的藝術家都不經常在此，大多只是到到現場，下達指令，就由義工及工作人員負責製作。有限的停駐，有限的交流，藝術家即使將當地特色融入創作意念，也很可能在不及消化的情況下，表面地再現越後妻有的故事，陷入近於 Spivak 擔心的弱勢無法發聲的局面。</p>
<p>代書始終是帶著距離的詮釋，與其由外地人操刀，增加誤讀風險，不如讓當地人主動參與其中。可是，該怎樣參與呢？越後妻有的六、七萬人口，大多對藝術認識不深。要求他們用藝術表達自己，未必是最有效的方法。與其邀請他們「一起藝術」，或許更有意義的方法是，讓他們用上最舒服的方式說自己的故事。當地好些藝術項目，就鄉鎮舊建築進行翻新活化，再邀請當地人管理。相對於藝術家的幕後製作，參與其中的當地人更是走在前線，直接接觸來訪人士。互動透過食物、對話促成，實驗「說故事也不必藝術」的假設。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/ch3-2_XuS9Q.png"><figcaption><span>負責管理《產土神之家》的其中一名當地女性，解說屋內的特色擺設。</span></figcaption></figure><p></p>
<p>距今近百年歷史的《產土神之家》，2006 年引入區外人士，對建築結構進行繕修，改造為餐廳，並聘請當地婦女，擔任廚師，運用當地種植的食材製作特色料理。記者到訪當日，天氣不穩，下著綿綿細雨，負責的歐巴桑早早就在屋外迎接，介紹藝術裝置之餘，又不忘解釋屋內的日本傳統建築特色。離開之前，她還俏皮地抓著我們合照，才願意「放行」。</p>
<p>至於 Christian Boltanski 和 Jean Kalman 聯手製作的《最後的教室》(Last Class)，則活化前東川國小校舍，為大型光影裝置的場地。該處的管理員現由當地人出任，向參觀的人補充延伸閱讀的材料。就像禮堂的銅電風扇和燈泡裝置，管理員伯伯向記者憶述，作品勾起他的戰後回憶，「當年資源緊絀，屋內照明就如現場晦暗。」</p>
<p>如此看來，缺乏當地藝術家的三年展，或者不是「滅聲」的證據，反而是順從當地人習慣的取捨。</p>
<p>「這些作品不是靠作者一個人，是由當地人共同創作出來的。」北川富朗在《北川富朗大地藝術祭：越後妻有三年展的10種創新思維》寫下如此豪情壯語，當中對於「創作」的定義，或者較我們想像的更開闊。老爺爺老奶奶細說越後妻有的生活，都是真誠的口述歷史，儘管沒有經過藝術化處理，但都是對自我身份的確認。這些故事，跟裝置、表演、繪畫呈現自我的「藝術」，其實沒有太大分別。</p>
<p>藝術在社區改造的語境下，不只是生活環境的裝飾，也是刺激變化發生的種子。藝術活化的場景，本來是一件藝術品；引入當地人分享生活經驗，更延伸出第二重意義，近於參與式藝術 (participatory art) 的充權。無論藝術家，還是當地農民，一同參與在三年一度的藝術祭，「共同創作」了一個越後妻有的形象。</p>
<p>&nbsp;</p>
<p>文／grace</p>
]]></content>
        <author>
            <name>立場報道</name>
            <uri>https://www.thestandnews.com/author/standnewsreport</uri>
        </author>
        <category label="藝術"/>
        <published>2015-07-27T04:53:57.000Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[【越後妻有 2】誰的故事？──廢校美術館的繪本和果實]]></title>
        <id>https://www.thestandnews.com/art/%E8%B6%8A%E5%BE%8C%E5%A6%BB%E6%9C%89-2-%E8%AA%B0%E7%9A%84%E6%95%85%E4%BA%8B-%E5%BB%A2%E6%A0%A1%E7%BE%8E%E8%A1%93%E9%A4%A8%E7%9A%84%E7%B9%AA%E6%9C%AC%E5%92%8C%E6%9E%9C%E5%AF%A6</id>
        <link href="https://www.thestandnews.com/art/%E8%B6%8A%E5%BE%8C%E5%A6%BB%E6%9C%89-2-%E8%AA%B0%E7%9A%84%E6%95%85%E4%BA%8B-%E5%BB%A2%E6%A0%A1%E7%BE%8E%E8%A1%93%E9%A4%A8%E7%9A%84%E7%B9%AA%E6%9C%AC%E5%92%8C%E6%9E%9C%E5%AF%A6"/>
        <updated>2016-02-22T11:12:18.000Z</updated>
        <summary type="html"><![CDATA[因收生不足而停辦，學校就此荒廢的個案，在香港也屢見不鮮。活化空置校舍的計劃，不斷出台，例如佛教大光中學變身為生活書院、坪洋公立學校的「空城計劃」、大埔官立中學的「表演藝術中心」計劃……原來的學生走了，剩下來的空間可以怎樣再利用？同樣是廢校處處的越後妻有，提供了參考的例子。十日町西南面的缽村，聚落全屬同一姓氏，住戶之間關係密切。山間的真田國小，早在第一屆大地藝術祭已經參與其中，作為法國藝術家 Br…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/6_d0aTz.png"><figcaption><span>《鉢與田島征三‧繪本與樹木果實的美術館》課室內的佈置，都是根據學校最後五名學生的真實經歷再想像創作。</span></figcaption></figure><p>因收生不足而停辦，學校就此荒廢的個案，在香港也屢見不鮮。活化空置校舍的計劃，不斷出台，例如佛教大光中學變身為生活書院、坪洋公立學校的「空城計劃」、大埔官立中學的「表演藝術中心」計劃……原來的學生走了，剩下來的空間可以怎樣再利用？同樣是廢校處處的越後妻有，提供了參考的例子。</p>
<p>十日町西南面的缽村，聚落全屬同一姓氏，住戶之間關係密切。山間的真田國小，早在第一屆大地藝術祭已經參與其中，作為法國藝術家 Bruno Mothon 留駐創作的場地。直到 2005 年，學校最後三名學生畢業離開，校舍隨之變成荒廢空間。藝術祭藝術總監北川富朗邀請大阪出生的插畫家──田島征三，將校舍活化成立體繪本。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/1_oaeIf.png"><figcaption><span>田島征三現場簡介創作，指作品每次展出都有新元素。</span></figcaption></figure><p></p>
<p>擅長繪畫兒童繪本的田島征三，作品多帶有鄉土情懷。北川邀請他來新潟農村越後妻有，大概不止是好朋友，也是適切人選的考慮。現居於伊豆半島的田島，數年前開始拓展平面以外的創作，以樹木果實製作名為「生命記憶」的裝置。真田國小活化而成的《鉢與田島征三‧繪本與樹木果實的美術館》，正好結合田島平面走向立體的繪本之路。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/5_z9705.png"><figcaption><span>活化小學禮堂為立體繪本</span></figcaption></figure><p></p>
<p>田島進駐真田國小之後，收集最後三名學生的故事，構想成現在美術館的主題。木材刷上鮮艷的顏料，組合成象徵三名末代學生──由紀、由佳和健太的人形。禮堂裡的人偶都繫上了鋼索，連接到校舍外的水池，隨著室外水壓改變，室內的人偶也會相應動起來。兩層校舍每一個房間，都放置田島的創意，訴說故事的不同章節。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/8_LDjRL.png"><figcaption><span>今年用和紙和竹篾新製作的「怪獸」</span></figcaption></figure><p></p>
<p>美術館自第四屆 2009 年正式對外開放，田島卻從沒停止豐富故事內容。今年新增的部分包括：用製成的怪獸；課室改建成的放映室，參觀者可以透過小孔，「偷窺」美術館裡故事的動畫；課室內的果實裝置。田島說，裝置使用的果實是「從我的住居地帶來，但不代表這區沒有，都是很普通的植物。」既然如此常見，何不捨遠取近？田島則沒有再詳細解釋。而 2015 年最重要延伸，當然是田島為三個小孩的故事寫上最終回。他在最後一間課室，製作飛龍形象的木雕，象徵畢業學生衝出校園，尋找未來。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/7_g8PuZ.png"><figcaption><span>象徵學生衝出校園的雕塑</span></figcaption></figure><p></p>
<p>「那三個孩子會回來嗎？」記者問。</p>
<p>「我們沒有邀請，但也歡迎他們來。」場地管理人天野貴子答。</p>
<p>來自崎玉縣的天野，2009 年曾參與藝術祭另一作品《蛻皮之家》的雕刻工作，婚後再回到越後妻有協助《繪本和果實美術館》。她的個人經歷，跟田島製作的立體繪本不謀而合，暗暗呈顯出藝術源流帶動的一股延續力量。兩人並非土生土長，卻年復年的在這裡付出，也許代表著越後妻一種無法拒絕的吸引力。然而，當地人都去哪裡？</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/3_Qkq7f.png"><figcaption><span>美術館今年新造的果實裝置</span></figcaption></figure><p></p>
<p>《繪本和果實美術館》的一草一木，是田島根據真田國小末代學生的經歷，再想像創作而成。當地人的故事是說出來了，但為甚麼他們需要一個代言人，而不是自己說自己的故事？越後妻有藝術祭多項作品也有類似的共通點，取材地道，題材也地區限定，惟創作者都非本地人：里山現代美術館中，瑞士藝術家 Gerda Steiner &amp; Jörg Lenzlinger 收集當地人的日常用品，拼湊成人工衛星的裝置；三重縣出生的中里和人又在「泥土博物館」(Soil Museum)，展出越後妻有古老地下水道的攝影作品等。這些作品都表述了區外藝術家對當地的詮釋，或以媒介的方式折射出當地人的生活，然而發聲的怎麼都不是越後妻有的人？</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/4_L7z8S.png"><figcaption><span>藝術家田島從家鄉帶來的果實</span></figcaption></figure><p></p>
<p>外來藝術家能夠尊重越後妻有的文化，願意以作品反映當地生活狀態，立意值得欣賞，然而創作傾向「為他們而做」(for the people) 的層次。越後妻有仍然處於客體的存在，被外來者閱讀再演繹，而未能取得自說自話的主體性，是令人感到可惜之處。</p>
<p>這到底是誰的大地藝術祭？「為他們而做」是真的有為他們嗎？要讓越後妻有的人真正擁有 (of the people) 這場盛會，光是當地故事被言說並不足夠，要讓他們以自己的方式表達自己，才是重建尊嚴和自信的核心。十年完滿，2015 年的第六屆有往前走一步嗎？</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/2_5Gm1W.png"><figcaption><span>義工幫忙為木材刷上顏色</span></figcaption></figure><p></p>
]]></content>
        <author>
            <name>立場報道</name>
            <uri>https://www.thestandnews.com/author/standnewsreport</uri>
        </author>
        <category label="藝術"/>
        <published>2015-07-16T14:35:25.000Z</published>
    </entry>
    <entry>
        <title type="html"><![CDATA[【越後妻有 1】誰的活化？──雪國藝術祭的前世今生]]></title>
        <id>https://www.thestandnews.com/art/%E8%B6%8A%E5%BE%8C%E5%A6%BB%E6%9C%89-%E8%AA%B0%E7%9A%84%E6%B4%BB%E5%8C%96-%E9%9B%AA%E5%9C%8B%E8%97%9D%E8%A1%93%E7%A5%AD%E7%9A%84%E5%89%8D%E4%B8%96%E4%BB%8A%E7%94%9F</id>
        <link href="https://www.thestandnews.com/art/%E8%B6%8A%E5%BE%8C%E5%A6%BB%E6%9C%89-%E8%AA%B0%E7%9A%84%E6%B4%BB%E5%8C%96-%E9%9B%AA%E5%9C%8B%E8%97%9D%E8%A1%93%E7%A5%AD%E7%9A%84%E5%89%8D%E4%B8%96%E4%BB%8A%E7%94%9F"/>
        <updated>2016-06-12T12:18:10.000Z</updated>
        <summary type="html"><![CDATA[老夫婦從車子下來，互相攙扶總算把車門關上。就在法國藝術家 Christian Lapie 的作品《砦 61》前，他們不是站著欣賞作品，而是彎腰下田。戴著闊大的帽子，臉龐都收在陰暗之中，只見雙手熟練地在泥土上翻來覆去……聽說，這片日本本島西北隅的土地，是日本文學家川端康成《雪國》的小說場景。地形氣候的緣故，新潟縣越後妻有一帶，一年下來，幾乎五個月都是下雪的季節。大雪冰封了生活，卻在飄雪的時候，潤澤…]]></summary>
        <content type="html"><![CDATA[<figure><img src="https://assets.thestandnews.com/media/photos/1_wwhgY.png"><figcaption><span>法國藝術家 Christian Lapie 的作品《砦 61》。</span></figcaption></figure><p>老夫婦從車子下來，互相攙扶總算把車門關上。就在法國藝術家 Christian Lapie 的作品《砦 61》前，他們不是站著欣賞作品，而是彎腰下田。戴著闊大的帽子，臉龐都收在陰暗之中，只見雙手熟練地在泥土上翻來覆去……</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/4_QdxM8.png"><figcaption><span>藝術品旁邊的老人，正在默默耕耘。</span></figcaption></figure><p></p>
<p>聽說，這片日本本島西北隅的土地，是日本文學家川端康成《雪國》的小說場景。地形氣候的緣故，新潟縣越後妻有一帶，一年下來，幾乎五個月都是下雪的季節。大雪冰封了生活，卻在飄雪的時候，潤澤大地。背靠里山，面臨日本最長河流信濃川，該區長久以來都是一大糧倉，不但培植出馳名的越光米，稻米產量也全國數一數二。</p>
<p><relatedarticlesblock data-count="3"></relatedarticlesblock></p>
<p>農業為越後妻有帶來光輝的日子，戰後區內人口更一度上升至 12 萬。隨著工業化和經濟轉型，一如世界各地的發展模式，年輕一代往大城市遷移。昔日熱鬧的農村，今天剩下寥落的老人和小孩。人去樓空：百年老屋無人居住，學校也因為收生不足而關閉。</p>
<p>自 1999 年，日本政府啟動「平成大合併」，就人口疏落的區域，透過行政區重新劃分，促進公共資源再分配。越後妻有也列入計劃之中，由原本 7 個行政區，重組成十日町和津南町兩區。新潟縣政府進一步活化人口老化的鄉鎮，推出「新‧新潟鄉鎮創生計劃」，強化地方復興的工作。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/2_rXfid.png"><figcaption><span>位於松代的農舞台，提供展覽空間，並作社區用途。</span></figcaption></figure><p></p>
<p>出生於新潟縣的策展人北川富朗，就在這個時候推出「大地藝術祭」的企劃，希望以三年一度的大型藝術節慶，為鄉村注入活力和生氣。藝術祭自 2000 年首辦以來，均在融雪之後的 7 至 9 月之間選取約 50 日來「過節」。負責策劃的主辦方，向世界各地招收藝術項目建議書之餘，亦會主動邀請著名藝術家到當地進行創作。當中不乏熟悉的名字，例如：草間彌生、Marina Abramovic、James Turrell 等。藝術祭刻意「徹底非效率、把藝術品到處散置在各個村莊」，抗衡講究效率的城市模式之餘，也藉此吸引參觀者走遍大地每一片角落。</p>
<p>考其策展的初衷，北川在《北川富朗大地藝術祭：越後妻有三年展的10種創新思維》，多次提到城鄉差異造成的問題，令當地人的生存尊嚴受到挫折，強調藝術祭的初衷，是為了甘願留在村莊終老的長者：</p>
<blockquote>
<p>「我想讓那些一戶戶人家逐漸消失的村落中的老爺爺和老奶奶有開心的回憶，即使只是短期間也好。這便是大地藝術祭的初衷。」</p>
<p>「想看見生活在越後妻有的爺爺奶奶的笑臉，想讓他們開心，想貼近他們的心情。」</p>
<p>「我們決定在因人口遞減無法舉行廟會的村落，舉行三年一次的『祭典』，讓老爺爺老奶奶參與。」</p>
</blockquote>
<p>人，是藝術祭誕生核心，當初想像的人文關懷，實際上能夠做到多少？三年一度的藝術祭，帶來數以十萬的人流，當中到底有多少正視這些「留守老人」？當記者實實在在走在越後妻有的鄉鎮農田，不禁質疑初衷與現實，距離究竟有多遠？</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/3_LaN6K.png"><figcaption><span>寥落的「松代商店街」。</span></figcaption></figure><p></p>
<p>松代火車站，以南是藝術祭重要據點之一的「農舞台」，也是多用途的社區空間，可作劇場，又有兒童遊樂設施，另設食堂和土產店；以北卻是村民住處，長長直直的所謂「商店街」，兩旁店鋪疏疏落落地開著。雜貨店沒有人，文具店沒有人，大街上也人跡罕見。北川富朗筆下的老爺爺老奶奶，一個一個坐在屋前；或架著助行器，在車路旁邊的小道，緩緩前行。</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/5_iEqrj.png"><figcaption><span>松代街頭，獨來獨往的留守者。</span></figcaption></figure><p></p>
<p>那可是開展前一個多月，這「松代商店街」一點熱鬧、期待、興奮的痕跡也不可見，一切歸於寂靜。火車軌道的鋪設，有意無意將小鎮劃分以南北兩邊，南岸藝術遍地開花，北岸的老人和小孩又有多少人關注？開展以後即使帶來人流，讓這裡變得遊人如鰂，又是不是當地人的意願？</p>
<p></p><figure><img src="https://assets.thestandnews.com/media/photos/6_1IuRr.png"><figcaption><span>松代火車站，以南有草間彌生的作品，以北則是居民住所。</span></figcaption></figure><p></p>
<p>人際關懷如何改善社區，或許難以量度，越後妻有大地藝術祭卻實實在在帶動了當地經濟和人流增長。以新潟縣建設投資及消費支出為例，上屆帶來合共帶來 46.5 億日元（即約 3 億港幣）的經濟波及效應（註一）。藝術祭亦增加當地的職業機會，合共提供約 400 個空缺，吸引部分青年留低發展，甚至外地青年返鄉建設。參觀人數亦按屆上升，從 2000 年第一屆的 16.2 萬，增加至 2012 年第五屆的 48.8 萬。</p>
<p>馬斯洛金字塔提出，人類最基本的需求是生理上的滿足。層層遞進，我們更需要安全感、情感、自尊，甚至自我實現。這場歷時超過十年的社會改造，期望透過藝術活化社區。帶來了收入，創造了就職，然後還實現了甚麼？當地人的歸屬感提升了嗎？較從前自信了嗎？他們的創作和思維得到體現了嗎？說到底，在雪國短暫的晴空下，舉辦為期 50 天的藝術祭，到底是為了誰人而做？藝術作品釋出的，是誰的故事？藝術祭發生的一切，又是誰的創作？參觀者又以誰的視角來看待？</p>
<p>以活化鄉鎮為名的一場藝術祭，到頭來又是誰的活化？</p>
<p>&nbsp;</p>
<p>──</p>
<p><strong>備註</strong></p>
<p>註一：根據日本<a href="http://www.mlit.go.jp/kankocho/zh-tw/siryou/toukei/kouka.html" target="_blank">觀光廳</a>的定義，經濟波及效應是指，旅遊帶動的收入，包括旅客住宿、飲食、交通、導覽、文化服務、休閒娛樂及觀光商品等開支。</p>
<p>&nbsp;</p>
<p>文／grace</p>
]]></content>
        <author>
            <name>立場報道</name>
            <uri>https://www.thestandnews.com/author/standnewsreport</uri>
        </author>
        <category label="藝術"/>
        <published>2015-07-14T11:35:10.000Z</published>
    </entry>
</feed>