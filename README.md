# About
Most files are downloaded from The Stand News (www.thestandnews.com), and are copyrighted the original author (see LICENSE).<br>
This repository is intended to be a back up version of www.thestandnews.com which was shut down on 29 December, 2021, due to arrests of high level personnel of the company.<br>
The maintainer(me) takes no responsibility and legal liability for any use of any files in this repository.<br>
# Download
You can, as always
```
git clone https://gitlab.com/standnews-backup/www.thestandnews.com.git
```
Alternatively you can download from [releases](https://gitlab.com/standnews-backup/www.thestandnews.com/-/releases/1.0). However the size would be larger.

# Deployment
Updated please use:
```
npm install
node server.js
```
this starts the server at localhost:2999
missing files will be downloaded from internet archive (if available)<br>
Some html files are without .html extension, please proceed with caution.<br>
Below are some solutions.<br>
express.js:
```
app.use(express.static(path.join(__dirname, 'public'),{index:false,extensions:['html']}));
```
nginx:
```
location /mr {
  default_type "text/html";
  try_files  /fullpath/$uri /fullpath/$uri.html /fullpath/$uri/index.html  /fullpath/index.html;
}
```
If you want to serve the site with images, you should also deploy [standnews-assets](https://gitlab.com/standnews-backup/standnews-assets), and change all image link hosts in htmls to your assets url.<br>
To quickly change the urls you can:
```
find ./ -type f -readable -writable -exec sed -i "s/assets.thestandnews.com/assets.example.com/g" {} \;
```
Most links are already set to http://localhost:2998 for local deployment.
# Contributions
If you find files missing in this repo (or assets) and you have those, please submit a merge request, or send a email to standnewsbackup@protonmail.com (not checked often). Thank you.
